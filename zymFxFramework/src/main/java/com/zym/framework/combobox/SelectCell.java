package com.zym.framework.combobox;

import com.zym.framework.model.SelectModel;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-02
 */
public class SelectCell implements Callback<ListView<SelectModel>, ListCell<SelectModel>> {
    @Override
    public ListCell<SelectModel> call(ListView<SelectModel> param) {
        return new ListCell<SelectModel>() {
            @Override
            protected void updateItem(SelectModel item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getValue());
            }

        };
    }
}
