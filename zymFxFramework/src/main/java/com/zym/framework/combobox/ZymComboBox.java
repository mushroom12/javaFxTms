package com.zym.framework.combobox;

import com.zym.application.exception.AppBugExcepiton;
import com.zym.framework.model.SelectModel;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-25
 */
public class ZymComboBox extends ComboBox<SelectModel> {
    public ZymComboBox(){
        SelectCell factory = new SelectCell();
        this.setCellFactory(factory);
        this.setButtonCell(factory.call(null));
    }
    public void initValue() {
        initValue(0);
    }
    public void initValue(int i) {
        ObservableList<SelectModel> items = this.getItems();
        if(items.size() == 0){
            return;
        }
        if(i>=items.size()){
            throw new AppBugExcepiton("i长度超出集合范围");
        }
        SelectModel selectModel = items.get(i);
        this.setValue(selectModel);
    }
}
