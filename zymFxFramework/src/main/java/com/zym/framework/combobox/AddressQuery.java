package com.zym.framework.combobox;

import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-03
 */
public interface AddressQuery {
    public List<String> query(String text);
}
