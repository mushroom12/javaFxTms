package com.zym.framework.combobox;

import com.zym.application.api.AmapMap;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 * 装饰者
 * @author
 * @create 2021-03-03
 */
public class AddressAutoComplete implements EventHandler<KeyEvent>, ChangeListener<Boolean> {
    private AutoComplete<String> autoComplete;
    private AddressQuery addressQuery;
    public AddressAutoComplete(AutoComplete<String> autoComplete){
        this(autoComplete,null);

    }
    public AddressAutoComplete(AutoComplete<String> autoComplete,AddressQuery addressQuery){
        this.autoComplete =autoComplete;
        if(addressQuery != null){
            this.addressQuery = addressQuery;
        }else {
            this.addressQuery = new AmapMap();
        }
        this.autoComplete.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent t) {
                autoComplete.hide();
            }
        });
        this.autoComplete.setChangeListener(this);
        this.autoComplete.setOnKeyReleased(this);
        this.autoComplete.setPromptText("输入回车查询地址……");
        this.autoComplete.setMapQuery(true);
    }
    @Override
    public void handle(KeyEvent event) {
        if( event.getCode() == KeyCode.ENTER){
            List<String> query = addressQuery.query(autoComplete.getText());
            if(query.size() > 0){
                autoComplete.setData(query);
            }
        }
        autoComplete.handle(event);
    }

    @Override
    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

    }
}
