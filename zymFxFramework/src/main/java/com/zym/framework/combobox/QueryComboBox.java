package com.zym.framework.combobox;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.utils.StringUtils;
import com.zym.framework.CallBack;
import com.zym.framework.model.SelectModel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.util.Callback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-02
 */
public class QueryComboBox extends ComboBox  {
    private String scanKey;
    private String url;
    protected   QueryComboBoxTableView queryComboBoxTableView;
    private Map requestMap;
    private String idKey;
    private String textKey;
    private SelectModel selectData;
    private ChangeListener changeListener;
    public QueryComboBox(){
        this.queryComboBoxTableView = new QueryComboBoxTableView(this);
        this.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                if(newValue){
                    query();
                }
            }
        });


        this.setOnKeyReleased(event -> {
            if (event.getCode() == KeyCode.ENTER){
                query();
                this.show();
            }
        });
        changeListener = getChonListerner();
        focusedProperty().addListener(changeListener);
    }

    public ChangeListener getChangeListener() {
        return changeListener;
    }


    private ChangeListener getChonListerner(){
       return new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                Object value = getValue();
                if(value == null){
                    setValue(null);
                    selectData = null;
                    return;
                }
                if(StringUtils.isEmpty(value.toString())){
                    setValue(null);
                    selectData = null;
                    return;
                }
                if(selectData != null){
                    setValue(selectData.getValue());
                }else {
                    setValue(null);
                }
            }
        };
    }

    @Override protected Skin<?> createDefaultSkin() {
        return queryComboBoxTableView;
    }

    protected void query(){
        HashMap hashMap = new HashMap();
        hashMap.put("listNumber",15);
        if(requestMap != null){
            hashMap.putAll(requestMap);
        }
        Object value = this.getValue();
        if(value!=null && value instanceof String){
            hashMap.put("sqlWhereNameToLike",scanKey);
            hashMap.put("sqlWhereValueToLike",value.toString());
        }
        ReturnModel returnModel = Http.postNotProgress(url, hashMap);
        Object obj = returnModel.getObj();
        if(obj != null){
            JSONArray jsonArray = (JSONArray) obj;
            queryComboBoxTableView.setData(jsonArray);
        }
    }

    public SelectModel getSelectData() {
        return selectData;
    }

    public void setSelectData(SelectModel selectData) {
        this.selectData = selectData;
        setValue(selectData.getValue());
    }

    public void initGrid(List<TableColumn> tableColumns, String scanKey, String url, Map requestMap, String idKey, String textKey){
        initGrid(tableColumns,scanKey,url,requestMap,idKey,textKey,null);
    }
    public void initGrid(List<TableColumn> tableColumns, String scanKey, String url, Map requestMap, String idKey, String textKey, CallBack cellBack){
        this.idKey = idKey;
        this.textKey = textKey;
        this.requestMap = requestMap;
        this.scanKey = scanKey;
        this.url = url;
        this.setEditable(true);//可编辑
        this.setPromptText("按回车搜索");
        queryComboBoxTableView.setColumns(tableColumns);
        queryComboBoxTableView.setRowDbOnlick(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        if(jsonObject != null){
                            SelectModel selectModel = new SelectModel();
                            selectModel.setValue(jsonObject.getString(textKey));
                            selectModel.setId(jsonObject.getString(idKey));
                            setValue(jsonObject.getString(textKey));
                            setSelectData(selectModel);
                            queryComboBoxTableView.hide();
                            if(cellBack != null){
                                cellBack.run(jsonObject);
                            }
                        }
                    }
                });
                return row ;
            }
        });
//        query();
    }
}
