package com.zym.framework.form;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public interface FormCallback {
    void submitEnd();
    void submitStart(JSONObject jsonObject);
    void getStart(Map map);
}
