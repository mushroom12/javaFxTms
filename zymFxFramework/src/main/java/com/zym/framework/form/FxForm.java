package com.zym.framework.form;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-20
 */
public interface FxForm extends Form {
    void submit();
    void clearError();
    void open(String id);
    void close();
//    void addButton(Button button);
}
