package com.zym.framework.notice;

import com.zym.application.service.PageService;
import com.zym.application.service.WinMainInfo;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-21
 */
public abstract class NoticeUtils {
    private static final Stage mainStage = PageService.getInstance().getMainStage();
    private static final Tooltip tooltip ;
    private static final PageService instance = PageService.getInstance();
    private static long time = 2300;
    private  static final Image successPng = new Image(NoticeUtils.class.getClass().getResourceAsStream("/component/icon/success.png") );
    private  static final Image questionPng = new Image(NoticeUtils.class.getClass().getResourceAsStream("/component/icon/question.png") );
    private  static final Image errorPng = new Image(NoticeUtils.class.getClass().getResourceAsStream("/component/icon/error.png") );
    private  static final Image wriningPng = new Image(NoticeUtils.class.getClass().getResourceAsStream("/component/icon/warning.png") );
    private static final ImageView imageView;
    static{
        imageView = new ImageView();
        imageView.setFitWidth(35);
        imageView.setFitHeight(35);
        tooltip= new Tooltip();
        tooltip.setMinWidth(330);
        tooltip.setMinHeight(80);
        tooltip.setStyle("-fx-font-size: 18;-fx-background-color:#F2F2F2;-fx-text-fill:#000000");
        tooltip.setGraphic(imageView);
        tooltip.setOnShowing(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Thread thread = new Thread(() -> {
                    try {
                        Thread.sleep(time);
                        if (tooltip.isShowing()) {
                            Platform.runLater(() -> tooltip.hide());
                        }
                    } catch (Exception exp) {
                        exp.printStackTrace();
                    }
                });
                thread.setDaemon(true);
                thread.start();
            }
        });
    }
    public static void  success(String text){
        imageView.setImage(successPng);
        showNotice(text);
    }
    public static void  warning(String text){
        imageView.setImage(wriningPng);
        showNotice(text);
    }
    public static void  error(String text){
        imageView.setImage(errorPng);
        showNotice(text);
    }
    public static void question(String text){
        imageView.setImage(questionPng);
        showNotice(text);
    }
    public static void  showNotice(String text){
        tooltip.setText("  "+text);
        WinMainInfo winMainInfo = instance.getWinMainInfo();
        tooltip.setX(winMainInfo.getX()+ winMainInfo.getWidth()-320);
        tooltip.setY(winMainInfo.getY()+winMainInfo.getHeight()-tooltip.getMinHeight());
        tooltip.show(mainStage);
    }

    //传说中的动画
    public static void startAnimation(Stage arg0) throws Exception {

        BorderPane root = new BorderPane();
        Scene scene = new Scene(root,400,400);

        Rectangle rectParallel = new Rectangle(10,200,50, 50);
        rectParallel.setArcHeight(15);
        rectParallel.setArcWidth(15);
        rectParallel.setFill(Color.DARKBLUE);
        rectParallel.setTranslateX(50);
        rectParallel.setTranslateY(75);

        root.getChildren().add(rectParallel);

        //定义矩形的淡入淡出效果
        FadeTransition fadeTransition=new FadeTransition(Duration.millis(2000), rectParallel);
        fadeTransition.setFromValue(1.0f);
        fadeTransition.setToValue(0.3f);
        fadeTransition.setCycleCount(2);
        fadeTransition.setAutoReverse(true);
        //fadeTransition.play();

        //定义矩形的平移效果
        TranslateTransition translateTransition=new TranslateTransition(Duration.millis(2000), rectParallel);
        translateTransition.setFromX(50);
        translateTransition.setToX(350);
        translateTransition.setCycleCount(2);
        translateTransition.setAutoReverse(true);
        //translateTransition.play();

        //并行执行动画
        ParallelTransition parallelTransition=new ParallelTransition(fadeTransition,
                translateTransition);
        parallelTransition.setCycleCount(Timeline.INDEFINITE);
        parallelTransition.play();
        arg0.setScene(scene);
        arg0.show();
    }

}
