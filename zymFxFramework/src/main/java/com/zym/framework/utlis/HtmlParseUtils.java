package com.zym.framework.utlis;

import com.zym.application.utils.StringUtils;
import com.zym.framework.model.HtmlModel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-23
 */
public abstract class HtmlParseUtils {
    public static HtmlModel parseHtml(String dataBookValue) {
        HtmlModel htmlModel = new HtmlModel();
        //分析是否是HTML
        if(dataBookValue.indexOf("<") ==1 && dataBookValue.lastIndexOf(">") == dataBookValue.length()-1){ //HTML
            Document document = Jsoup.parse(dataBookValue);
            Elements span_ = document.getElementsByTag("span");
            if(span_ != null){
                if(span_.size() == 1){
                    Element element = span_.get(0);
                    String html = element.html();
                    htmlModel.setValue(html.trim());
                    Attributes attributes = element.attributes();
                    String style = attributes.get("style");
                    if(StringUtils.isNotEmpty(style)){
                        String[] split = style.split(",");
                        for (int i = 0; i <split.length ; i++) {
                            String sp = split[i];
                            String[] data = sp.split(":");
                            if(data[0].equals("color") && data.length ==2){
                                htmlModel.setColor(data[1].trim());
                            }
                        }
                    }

                }
            }



        }else {
            htmlModel.setValue(dataBookValue);
        }
        return htmlModel;
    }
}
