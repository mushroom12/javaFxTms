package com.zym.framework.progress;

import javafx.concurrent.Task;

public abstract class ProgressTask<T> extends Task<T> {

    @Override
    protected T call() throws Exception {
        return execute();
    }

    @Override
    public void updateMessage(String message) {
        super.updateMessage(message);
    }

    public void updateInfo(long workDone, long max,String message){
        super.updateMessage(message);
        super.updateProgress(workDone,max);
    }


    protected abstract T execute() throws Exception;
}
