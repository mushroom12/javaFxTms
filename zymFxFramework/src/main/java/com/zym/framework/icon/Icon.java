package com.zym.framework.icon;

import javafx.scene.image.Image;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-08
 */
public class Icon {
    public static final Image ADD = new Image(Icon.class.getClass().getResourceAsStream("/component/icon/add.png") );
    public static final Image CENTEL = new Image(Icon.class.getClass().getResourceAsStream("/component/icon/centel.png") );
}
