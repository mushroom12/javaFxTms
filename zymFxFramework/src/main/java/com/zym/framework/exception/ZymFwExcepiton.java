package com.zym.framework.exception;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-03
 */
public class ZymFwExcepiton extends RuntimeException {

    public ZymFwExcepiton() {
    }

    public ZymFwExcepiton(String message) {
        super(message);
    }

    public ZymFwExcepiton(String message, Throwable cause) {
        super(message, cause);
    }

    public ZymFwExcepiton(Throwable cause) {
        super(cause);
    }
}
