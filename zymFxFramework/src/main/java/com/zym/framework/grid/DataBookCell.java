package com.zym.framework.grid;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.utils.StringUtils;
import com.zym.framework.model.HtmlModel;
import com.zym.framework.utlis.IviewParseUtils;
import javafx.beans.NamedArg;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.paint.Color;
import javafx.util.Callback;

import java.util.Map;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-23
 */
public class DataBookCell<S,T> implements Callback<TableColumn<S,T>, TableCell<S,T>> {
        private final Object key;
        private final JSONObject jsonObject;

        /**
         * Creates a default MapValueFactory, which will use the provided key to
         * lookup the value for cells in the {@link TableColumn} in which this
         * MapValueFactory is installed (via the
         * {@link TableColumn#cellValueFactoryProperty() cell value factory} property.
         *
         * @param key The key to use to lookup the value in the {@code Map}.
         */
        public DataBookCell(JSONObject jsonObject , final @NamedArg("key") Object key) {
            this.jsonObject = jsonObject;
            this.key = key;
        }
    @Override
    public TableCell<S, T> call(TableColumn<S, T> param) {
        TableCell cell=new TableCell() {
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                this.setText(null);
                this.setGraphic(null);
                if (!empty) {
                    this.setAlignment(Pos.CENTER);
                    ObservableList tableData =param.getTableView().getItems();
                    Map map = (Map) tableData.get(this.getIndex());
                    Object o = map.get(key);
                    if(o != null){
                        String dataBookValue = jsonObject.getString(o.toString());
                        HtmlModel testValue = IviewParseUtils.parseHtml(dataBookValue);
                        this.setText(testValue.getValue());

                        if (StringUtils.isNotEmpty(testValue.getColor()))
                            this.setTextFill( Color.valueOf(testValue.getColor()));
                    }

                }
            }

        };
        return cell;
    }

}