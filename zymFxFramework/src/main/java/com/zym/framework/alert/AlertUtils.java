package com.zym.framework.alert;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.util.Optional;

/**
 * QQ 165149324
 * 臧英明
 * 警告工具
 * @author
 * @create 2021-01-30
 */
public abstract class AlertUtils {
    private static final Alert alert = new Alert(Alert.AlertType.ERROR);
    public static void  success(String text){
        showAlert(Alert.AlertType.INFORMATION,text);
    }
    public static void  warning(String text){
        showAlert(Alert.AlertType.WARNING,text);
    }
    public static void  error(String text){
        showAlert(Alert.AlertType.ERROR,text);
    }

    public static  void showAlert(Alert.AlertType alertType,String text){
        alert.setAlertType(alertType);
        alert.setContentText(text);
        alert.show();
    }
    public static boolean confirmYesNoConfirmation(String title, String message) {
        return confirm(Alert.AlertType.CONFIRMATION, title, message, ButtonType.YES, ButtonType.NO) == ButtonType.YES;
    }
    public static boolean confirmYesNoWarning(String title, String message) {
        return confirm(Alert.AlertType.WARNING, title, message, ButtonType.YES, ButtonType.NO) == ButtonType.YES;
    }
    public static ButtonType confirm(Alert.AlertType alertType, String title, String message, ButtonType... buttonTypes) {
        try {
            Alert alert = new Alert(alertType, message, buttonTypes);
            alert.setTitle(title);
            alert.setHeaderText((String)null);
            Stage stage = (Stage)alert.getDialogPane().getScene().getWindow();
//            FxApp.setupIcon(stage);
//            FxApp.setupModality(alert);
            Optional<ButtonType> result = alert.showAndWait();
            return (ButtonType)result.orElse(ButtonType.CANCEL);
        } catch (Exception var7) {
            var7.printStackTrace();
            return ButtonType.CANCEL;
        }
    }

}
