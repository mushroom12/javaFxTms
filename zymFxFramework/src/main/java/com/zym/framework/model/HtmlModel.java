package com.zym.framework.model;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-23
 */
public class HtmlModel {
    private String value;
    private String color;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
