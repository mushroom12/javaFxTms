package com.zym.framework.model;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-25
 */
public class SelectModel implements Cloneable{
    private String id;
    private String value;
    private int level;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    @Override
    public SelectModel clone()  {
        try {
            return (SelectModel) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
