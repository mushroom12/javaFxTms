package com.zym.application.supplis;

import com.alibaba.fastjson.JSONArray;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.framework.CallBack;
import com.zym.framework.combobox.QueryComboBox;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.model.ParseModel;
import com.zym.framework.model.SelectModel;
import com.zym.framework.utlis.IviewParseUtils;
import javafx.beans.value.ChangeListener;

import java.util.HashMap;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-08
 */
public class ProductScan extends QueryComboBox {
    private ZymComboBox baseItem;
    public ProductScan(ZymComboBox baseItem, CallBack callBack){
        super();
        ParseModel parseModel = IviewParseUtils.parseGrid(BaseData.CSPRODUCT, null);
        this.baseItem =baseItem;
        this.initGrid(parseModel.getTableColumns(), "NAME", null, null, "ID", "NAME",callBack);
        focusedProperty().removeListener(getChangeListener());
    }
    public void addChangeListener(ChangeListener changeListener){
        focusedProperty().addListener(changeListener);
    }
    @Override
    protected void query(){
        HashMap hashMap = new HashMap();
        hashMap.put("listNumber",15);
        Object value = this.getValue();
        if(value != null){
            hashMap.put("name",value.toString());
        }
        SelectModel value1 = baseItem.getValue();
        if(value1 != null &&  value1.getId() != null){
            hashMap.put("id",value1.getId());
        }
        ReturnModel returnModel = Http.postNotProgress("/baseproduct/initTableSelect", hashMap);
        Object obj = returnModel.getObj();
        if(obj != null){
            JSONArray jsonArray = (JSONArray) obj;
            queryComboBoxTableView.setData(jsonArray);
        }
    }

}
