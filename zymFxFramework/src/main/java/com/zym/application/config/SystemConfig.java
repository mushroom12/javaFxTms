package com.zym.application.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import javafx.stage.Stage;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 * 系统配置类
 * @author
 * @create 2021-01-27
 */
public  class SystemConfig {
    private  String sourcePath = null;
    private static final SystemConfig systemConfig;
    private  String  xmlConfig= null;
    private String baseUrl;
    private String loginName= "iasyn-Tms管理系统登录 客户端";
    private String logoPath;
    private String defaultOrgId;
    private String defaultUserName;
    private String defaultPassword;
    private JSONObject dataBook;
    private JSONObject orgConfig;
    private JSONObject systemData;
    private JSONObject loginData;
    private String recallPassword;
    private List<Element> children;
    private Stage stage;
    private Document document;
    static{
        systemConfig = new SystemConfig();
    }
    public static SystemConfig getInstance(){
        return systemConfig;
    }
    private SystemConfig(){

    }


    public void initXmlConfig(){
        String path = SystemConfig.class.getResource("/com").getPath(); //这个实际部署报错
        sourcePath= path.substring(0,path.lastIndexOf("com"));
        xmlConfig = sourcePath+"config.xml";
        SAXBuilder saxBuilder = new SAXBuilder();
        //2.创建输入流
        InputStream is = null;
        try {
            is = SystemConfig.class.getClassLoader().getResourceAsStream("config.xml");
            document = saxBuilder.build(is);
            Element rootElement = document.getRootElement();
            children = rootElement.getChildren();
            for (Element child : children) {
                String text = child.getText();
                String name = child.getName();
                if("baseUrl".equals(name)){
                    baseUrl = text;
                }
                if("loginName".equals(name)){
                    loginName = text;
                }
                if("logoPath".equals(name)){
                    logoPath = text;
                }
                if("defaultOrgId".equals(name)){
                    defaultOrgId = text;
                }
                if("defaultUserName".equals(name)){
                    defaultUserName = text;
                }
                if("defaultPassword".equals(name)){
                    defaultPassword = text;
                }
                if("recallPassword".equals(name)){
                    recallPassword = text;
                }
            }
            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //3.将输入流加载到build中

        //4.获取根节点
    }

    public String isRecallPassword() {
        return recallPassword;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public String getDefaultOrgId() {
        return defaultOrgId;
    }

    public String getDefaultUserName() {
        return defaultUserName;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public JSONObject getDataBook() {
        return dataBook;
    }

    public void setDataBook(JSONObject dataBook) {
        this.dataBook = dataBook;
    }

    public JSONObject getOrgConfig() {
        return orgConfig;
    }

    public void setOrgConfig(JSONObject orgConfig) {
        this.orgConfig = orgConfig;
    }

    public JSONObject getSystemData() {
        return systemData;
    }

    public void setSystemData(JSONObject systemData) {
        this.systemData = systemData;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setXmlUserInfo(String code,String username) {
        defaultUserName =username;
        defaultOrgId =code;
    }
    public void setXmlPassword(String password) {
        defaultPassword = password;
    }

    public void setXmlRecallPassword(boolean recallPassword) {
        if(recallPassword){
            this.recallPassword = "yes";
        }else {
            this.recallPassword = "no";
        }
    }

    public boolean getXmlRecallPassword() {
        if("yes".equals(recallPassword)){
           return true;
        }else {
           return false;
        }
    }

    public void updateXml(){
        Element rootElement = document.getRootElement();
        children = rootElement.getChildren();
        for (Element child : children) {
            String text = child.getText();
            String name = child.getName();
            if("baseUrl".equals(name)){
                child.setText(baseUrl);
            }
            if("loginName".equals(name)){
                child.setText(loginName);
            }
            if("logoPath".equals(name)){
                child.setText(logoPath);
            }
            if("defaultOrgId".equals(name)){
                child.setText(defaultOrgId);
            }
            if("defaultUserName".equals(name)){
                child.setText(defaultUserName);
            }
            if("defaultPassword".equals(name)){
                child.setText(defaultPassword);
            }
            if("recallPassword".equals(name)){
                child.setText(recallPassword);
            }
        }
        Format format = Format.getPrettyFormat();
        format.setEncoding("utf-8");//设置编码
        format.setIndent("    ");//设置缩进
        XMLOutputter out = new XMLOutputter(format);
        //把数据输出到xml中

        try {
            String s = out.outputString(document);//或者FileWriter
            FileWriter fileWriter = new FileWriter(xmlConfig);
            fileWriter.write(s);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setLoginData(JSONObject loginData) {
        this.loginData = loginData;
    }

    public JSONArray getMenuData() {
        return loginData.getJSONArray("menu");
    }
    public String getUserName(){
        return loginData.getString("NAME");
    }


}
