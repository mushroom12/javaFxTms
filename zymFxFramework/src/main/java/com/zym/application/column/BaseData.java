package com.zym.application.column;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public interface BaseData {
    String BASE_PRODUCT_TYPE=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 120,\n" +
            "            query:'text'\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 120,\n" +
            "            query:'text'\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 170,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 170,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:'text',\n" +
            "            width: 250,\n" +
            "          },\n" +
            "        ]";
    String BASE_UNIT=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String BaseCustomerSupplier = "[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '客户名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 180,\n" +
            "            query:\"text\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '客户编码',\n" +
            "            key: 'CODE',\n" +
            "            width: 180,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '是否是客户',\n" +
            "            key: 'CUSTOMER',\n" +
            "            width: 110,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '是否是承运商',\n" +
            "            key: 'SUPPLIER',\n" +
            "            width: 110,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '对账周期',\n" +
            "            key: 'CHECK_ACCONUNTS_PERIOD',\n" +
            "            width: 140,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '结算周期',\n" +
            "            key: 'SETTLE_ACCOUNTS_PERIOD',\n" +
            "            width: 140,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '销售负责人',\n" +
            "            key: 'SELL_PERSION',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            width: 200,\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String BaseTaxrate = "[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 120\n" +
            "          },\n" +
            "          {\n" +
            "            title: '税率',\n" +
            "            key: 'TAX_TATE',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String BASE_VEHICLE_TYPE=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '车型',\n" +
            "            key: 'CODE',\n" +
            "            width: 200,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String BASE_COST_MANAGE="[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '类型',\n" +
            "            key: 'TYPE',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '默认税率',\n" +
            "            key: 'TAXTATE',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '归类科目号',\n" +
            "            key: 'CLASSIFIED_SUBJECT',\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String BASE_ZONE="[\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            sortable: true\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            sortable: true,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '经纬度',\n" +
            "            key: 'LATITUDE',\n" +
            "            sortable: true\n" +
            "          },\n" +
            "          {\n" +
            "            title: '排序',\n" +
            "            key: 'SORT',\n" +
            "            sortable: true\n" +
            "          },\n" +
            "        ]";
    String BASE_RANDC = "[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '客户名称',\n" +
            "            key: 'BASE_CUSTOMER_SUPPLIER_NAME',\n" +
            "            width: 190,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"b.NAME\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '单位名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.NAME\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '姓名',\n" +
            "            key: 'PERSION',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.PERSION\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '电话',\n" +
            "            key: 'PHONE',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.PHONE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '区域',\n" +
            "            key: 'AREA',\n" +
            "            width: 220,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.AREA\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '地址',\n" +
            "            key: 'ADDRESS',\n" +
            "            width: 270,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.ADDRESS\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            width: 270,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String CSQUERY = "[\n" +
            "            {\n" +
            "              title:'序号',\n" +
            "              type: 'index',\n" +
            "              width: 60,\n" +
            "              align: 'center',\n" +
            "            },\n" +
            "            {\n" +
            "              title: '客户名称',\n" +
            "              key: 'NAME',\n" +
            "              width: 170,\n" +
            "              query:\"text\"\n" +
            "\n" +
            "            },\n" +
            "            {\n" +
            "              title: '客户编码',\n" +
            "              key: 'CODE',\n" +
            "              width: 160,\n" +
            "              query:\"text\"\n" +
            "            },\n" +
            "          ]";
    String BASE_ITEM= "[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '所属客户',\n" +
            "            key: 'BASE_CUSTOMER_SUPPLIER_NAME',\n" +
            "            width: 180,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 140,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.NAME\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 140,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.CODE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '销售负责人',\n" +
            "            key: 'SALE',\n" +
            "            width: 100,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '默认配载方式',\n" +
            "            key: 'LTL',\n" +
            "            width: 140,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '默认运输方式',\n" +
            "            key: 'SALE',\n" +
            "            width: 140,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "          },\n" +
            "        ]";
    String BASE_PRODUCT=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '所属项目',\n" +
            "            key: 'BASE_ITEM_NAME',\n" +
            "            width: 160,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"b.NAME\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '商品名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 160,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.NAME\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '编码',\n" +
            "            key: 'CODE',\n" +
            "            width: 160,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.CODE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '单位',\n" +
            "            key: 'UNIT',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 80,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 80,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 80,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 80,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            width: 200,\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.DESCRIPTION\"\n" +
            "          },\n" +
            "        ]";
    String BASE_KPI=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '项目名称',\n" +
            "            key: 'BASE_ITEM_ID',\n" +
            "            width: 190,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货区域',\n" +
            "            key: 'FAREA',\n" +
            "            width: 230,\n" +
            "            query:\"FAREA\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货区域',\n" +
            "            key: 'SAREA',\n" +
            "            width: 230,\n" +
            "            query:\"SAREA\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货时长',\n" +
            "            key: 'DESPATCH_LENGTH',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货时长',\n" +
            "            key: 'ARRIVE_LENGTH',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            width: 270,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String TMS_ORDER="[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '项目',\n" +
            "            key: 'BASE_ITEM_NAME',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"c.NAME\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '订单号',\n" +
            "            key: 'CODE',\n" +
            "            width: 150,\n" +
            "            queryIndex:\"a.CODE\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '提货',\n" +
            "            key: 'TASK',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '送货',\n" +
            "            key: 'DELIVER',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '客户单号',\n" +
            "            key: 'COSTOMER_CODE',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"a.COSTOMER_CODE\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发车批次',\n" +
            "            key: 'TMS_SHIPMENT_CODE',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"e.CODE\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '订单时间',\n" +
            "            key: 'TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '跟踪方式',\n" +
            "            key: 'TRACK_MODEL',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求发货时间',\n" +
            "            key: 'ESTIMATE_DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '实际发货时间',\n" +
            "            key: 'DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求到货时间',\n" +
            "            key: 'ESTIMATE_ARRIVE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '实际到货时间',\n" +
            "            key: 'ARRIVE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '运输方式',\n" +
            "            key: 'TRANSPORT_TYPE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '配载方式',\n" +
            "            key: 'LTL',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货单位',\n" +
            "            key: 'SNAME',\n" +
            "            width: 180,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货人',\n" +
            "            key: 'SPERSION',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货电话',\n" +
            "            key: 'SPHONE',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货地址',\n" +
            "            key: 'SADDRESS',\n" +
            "            width: 230,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货数量',\n" +
            "            key: 'REFUSE_NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '未收数量',\n" +
            "            key: 'UNCOLLECTED_NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            queryIndex:\"a.DESCRIPTION\",\n" +
            "          },\n" +
            "        ]";
    String TMS_ORDER_PRODUCT =" [\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货品代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 180,\n" +
            "            align: 'center',\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymQueryInput, {\n" +
            "                props: {\n" +
            "                  where:{id:self.item},\n" +
            "                  url:\"/baseproduct/initTableSelect\",\n" +
            "                  // where:{name:self.productsData[params.index].CODE},\n" +
            "                  columns:this.selectProductColumns,\n" +
            "                  tableWidth:590,\n" +
            "                  width:155,\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.CODE,\n" +
            "                  mainKey:\"CODE\"\n" +
            "                },\n" +
            "                style: {\n" +
            "                'text-align':'right'\n" +
            "                },\n" +
            "                on:{\n" +
            "                  onClickTableRow:function (event) {\n" +
            "                    self.productsData[params.index].NAME = event.NAME\n" +
            "                    self.productsData[params.index].CODE = event.CODE\n" +
            "                    self.productsData[params.index].UNIT = event.UNIT\n" +
            "                    self.productsData[params.index].NUMBER = 1\n" +
            "                    self.productsData[params.index].VALUE = event.VALUE\n" +
            "                    self.productsData[params.index].ROUGT_WEIGHT = event.ROUGT_WEIGHT\n" +
            "                    self.productsData[params.index].NET_WEIGHT = event.NET_WEIGHT\n" +
            "                    self.productsData[params.index].VOLUME = event.VOLUME\n" +
            "                    // if(!self.productsData[params.index].action){\n" +
            "                    //   self.productsData[params.index].action = \"update\" //标记字段\n" +
            "                    // }\n" +
            "                    var isInt = 0;\n" +
            "                    for (let i = 0; i <self.productsData.length ; i++) {\n" +
            "                      var data = self.productsData[i]\n" +
            "                      if(data.CODE){\n" +
            "                        isInt++\n" +
            "                      }\n" +
            "                    }\n" +
            "                    if(isInt == self.productsData.length){\n" +
            "                      self.productsData.push({\n" +
            "                        ID:null,\n" +
            "                        CODE:null,\n" +
            "                        NAME:null,\n" +
            "                        UNIT:null,\n" +
            "                        NUMBER:0,\n" +
            "                        ROUGT_WEIGHT:0,\n" +
            "                        NET_WEIGHT:0,\n" +
            "                        VOLUME:0,\n" +
            "                        VALUE:0,\n" +
            "                      })\n" +
            "                    }\n" +
            "                  },\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].CODE = event\n" +
            "                    var isInt = 0;\n" +
            "                    for (let i = 0; i <self.productsData.length ; i++) {\n" +
            "                      var data = self.productsData[i]\n" +
            "                      if(data.CODE){\n" +
            "                        isInt++\n" +
            "                      }\n" +
            "                    }\n" +
            "                    if(isInt == self.productsData.length){\n" +
            "                      self.productsData.push({\n" +
            "                        ID:null,\n" +
            "                        CODE:null,\n" +
            "                        NAME:null,\n" +
            "                        UNIT:null,\n" +
            "                        NUMBER:0,\n" +
            "                        ROUGT_WEIGHT:0,\n" +
            "                        NET_WEIGHT:0,\n" +
            "                        VOLUME:0,\n" +
            "                        VALUE:0,\n" +
            "                      })\n" +
            "                    }\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货品名称',\n" +
            "            key: 'NAME',\n" +
            "            align: 'center',\n" +
            "            width: 160,\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymTableInput, {\n" +
            "                props: {\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.NAME\n" +
            "                },\n" +
            "                on:{\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].NAME = event\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '单位',\n" +
            "            key: 'UNIT',\n" +
            "            align: 'center',\n" +
            "            width: 90,\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymTableInput, {\n" +
            "                props: {\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.UNIT\n" +
            "                },\n" +
            "                on:{\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].UNIT = event\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymTableInput, {\n" +
            "                props: {\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.NUMBER,\n" +
            "                  type:'number'\n" +
            "                },\n" +
            "                on:{\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].NUMBER = event\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymTableInput, {\n" +
            "                props: {\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.ROUGT_WEIGHT,\n" +
            "                  type:'number'\n" +
            "                },\n" +
            "                on:{\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].ROUGT_WEIGHT = event\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymTableInput, {\n" +
            "                props: {\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.NET_WEIGHT,\n" +
            "                  type:'number'\n" +
            "                },\n" +
            "                on:{\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].NET_WEIGHT = event\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymTableInput, {\n" +
            "                props: {\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.VOLUME,\n" +
            "                  type:'number'\n" +
            "                },\n" +
            "                on:{\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].VOLUME = event\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "            render: (h, params) => {\n" +
            "              var self = this\n" +
            "              return h(ZymTableInput, {\n" +
            "                props: {\n" +
            "                  disabled:self.disabled,\n" +
            "                  value: params.row.VALUE,\n" +
            "                  type:'number'\n" +
            "                },\n" +
            "                on:{\n" +
            "                  input:function (event) {\n" +
            "                    self.productsData[params.index].VALUE = event\n" +
            "                  }\n" +
            "                }\n" +
            "              })\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '清空',\n" +
            "            // slot: 'action',\n" +
            "            width: 80,\n" +
            "            align: 'center',\n" +
            "            renderHeader:(h, params) => {\n" +
            "              var self = this\n" +
            "              if(!self.view){\n" +
            "                return h('div', [\n" +
            "                  h('Button', {\n" +
            "                    props: {\n" +
            "                      disabled:self.disabled,\n" +
            "                      type:\"error\",\n" +
            "                      size:\"small\"\n" +
            "                    },\n" +
            "                    on:{\n" +
            "                      click:function (event) {\n" +
            "                        self.productsData = [{\n" +
            "                          ID:null,\n" +
            "                          CODE:null,\n" +
            "                          NAME:null,\n" +
            "                          UNIT:null,\n" +
            "                          NUMBER:1,\n" +
            "                          ROUGT_WEIGHT:0,\n" +
            "                          NET_WEIGHT:0,\n" +
            "                          VOLUME:0,\n" +
            "                          VALUE:0,\n" +
            "                        }]\n" +
            "                      }\n" +
            "                    }\n" +
            "                  },\"清空\"),\n" +
            "                ]);\n" +
            "              }\n" +
            "            },\n" +
            "            render:(h, params) => {\n" +
            "              var self = this\n" +
            "              if(!self.view){\n" +
            "                return h('div', [\n" +
            "                  h('Button', {\n" +
            "                    props: {\n" +
            "                      disabled:self.disabled,\n" +
            "                      type:\"error\",\n" +
            "                      size:\"small\"\n" +
            "                    },\n" +
            "                    on:{\n" +
            "                      click:function (event) {\n" +
            "                        if(self.productsData.length < 2){\n" +
            "                          self.productsData = [{\n" +
            "                            ID:null,\n" +
            "                            CODE:null,\n" +
            "                            NAME:null,\n" +
            "                            UNIT:null,\n" +
            "                            NUMBER:1,\n" +
            "                            ROUGT_WEIGHT:0,\n" +
            "                            NET_WEIGHT:0,\n" +
            "                            VOLUME:0,\n" +
            "                            VALUE:0,\n" +
            "                          }]\n" +
            "                        }else {\n" +
            "                          self.productsData.splice(params.index, 1) //删除\n" +
            "                          if(self.productsData .length == 0){\n" +
            "                            self.productsData = [{\n" +
            "                              ID:null,\n" +
            "                              CODE:null,\n" +
            "                              NAME:null,\n" +
            "                              UNIT:null,\n" +
            "                              NUMBER:1,\n" +
            "                              ROUGT_WEIGHT:0,\n" +
            "                              NET_WEIGHT:0,\n" +
            "                              VOLUME:0,\n" +
            "                              VALUE:0,\n" +
            "                            }]\n" +
            "                          }\n" +
            "                        }\n" +
            "                      }\n" +
            "                    }\n" +
            "                  },\"删除\"),\n" +
            "                ]);\n" +
            "              }\n" +
            "\n" +
            "            }\n" +
            "          },\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "            edit: 'text',\n" +
            "          },\n" +
            "        ]";
    String CSPRODUCT = "[\n" +
            "          {\n" +
            "            title: '货品编码',\n" +
            "            key: 'CODE',\n" +
            "            width: 130,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货品名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 140,\n" +
            "            align: 'center',\n" +
            "            ellipsis:true,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 80,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 80,\n" +
            "            align: 'center',\n" +
            "          }, {\n" +
            "            title: '体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 80,\n" +
            "            align: 'center',\n" +
            "          }, {\n" +
            "            title: '货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 80,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "        ]";
    String USER=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "            fixed:\"left\"\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"user.NAME\",\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '登陆账户',\n" +
            "            key: 'PHONE',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"user.PHONE\",\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '电子邮箱',\n" +
            "            key: 'EMAIL',\n" +
            "            width: 220,\n" +
            "            queryIndex:\"user.EMAIL\",\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '部门',\n" +
            "            key: 'SYS_DEPT_NAME',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"dept.NAME\",\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '岗位',\n" +
            "            key: 'SYS_STATION_NAME',\n" +
            "            queryIndex:\"station.NAME\",\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String ORG="[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "            fixed:\"left\"\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 230,\n" +
            "            query:\"text\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '电话',\n" +
            "            key: 'PHONE',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '负责人',\n" +
            "            key: 'PERSION',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '地址',\n" +
            "            key: 'ADDRESS',\n" +
            "            width: 220,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '经纬度',\n" +
            "            key: 'LTL',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String ROLE=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '名称',\n" +
            "            key: 'NAME',\n" +
            "            width: 120,\n" +
            "            query:\"text\"\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\",\n" +
            "          },\n" +
            "        ]";
    String OPERATION = "[\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发车批次',\n" +
            "            key: 'CODE',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.CODE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '关联单号',\n" +
            "            key: 'TMS_ORDER_CODE',\n" +
            "            width: 200,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"b.CODE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发运时间',\n" +
            "            key: 'DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '车牌号',\n" +
            "            key: 'LICENSE_NUMBER',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.LICENSE_NUMBER\"\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '司机',\n" +
            "            key: 'DRIVER',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.DRIVER\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '司机电话',\n" +
            "            key: 'DRIVER_PHONE',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.DRIVER_PHONE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '跟踪方式',\n" +
            "            key: 'TRACK_MODEL',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          // {\n" +
            "          //   title: '当前进度',\n" +
            "          //   key: 'NUMBER',\n" +
            "          //   width: 150,\n" +
            "          //   align: 'center',\n" +
            "          // },\n" +
            "          // {\n" +
            "          //   title: '要求到货时间',\n" +
            "          //   key: 'ESTIMATE_ARRIVE_TIME',\n" +
            "          //   width: 150,\n" +
            "          // },\n" +
            "          // {\n" +
            "          //   title: '剩余时效',\n" +
            "          //   key: 'ESTIMATE_ARRIVE_TIME',\n" +
            "          //   width: 150,\n" +
            "          // },\n" +
            "          {\n" +
            "            title: '运输方式',\n" +
            "            key: 'TRANSPORT_TYPE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            queryIndex:\"a.DESCRIPTION\",\n" +
            "          },]";
    String TMS_ABNORMAL = " [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '异常类型',\n" +
            "            key: 'TYPE',\n" +
            "            width: 120,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '异常时间',\n" +
            "            key: 'TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '异常信息',\n" +
            "            key: 'TEXT',\n" +
            "            width: 700,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '审核人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '审核时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
    String SHIPMENT = " [\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '项目',\n" +
            "            key: 'BASE_ITEM_NAME',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"c.NAME\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '订单号',\n" +
            "            key: 'CODE',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '客户单号',\n" +
            "            key: 'COSTOMER_CODE',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"a.COSTOMER_CODE\",\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '订单时间',\n" +
            "            key: 'TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求发货时间',\n" +
            "            key: 'ESTIMATE_DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求到货时间',\n" +
            "            key: 'ESTIMATE_ARRIVE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '运输方式',\n" +
            "            key: 'TRANSPORT_TYPE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '配载方式',\n" +
            "            key: 'LTL',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货单位',\n" +
            "            key: 'FNAME',\n" +
            "            width: 180,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货人',\n" +
            "            key: 'FPERSION',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货电话',\n" +
            "            key: 'FPHONE',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货地址',\n" +
            "            key: 'FADDRESS',\n" +
            "            width: 230,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货单位',\n" +
            "            key: 'SNAME',\n" +
            "            width: 180,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货人',\n" +
            "            key: 'SPERSION',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货电话',\n" +
            "            key: 'SPHONE',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货地址',\n" +
            "            key: 'SADDRESS',\n" +
            "            width: 230,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            queryIndex:\"a.DESCRIPTION\",\n" +
            "          },\n" +
            "        ]";
    String BACK = " [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '项目',\n" +
            "            key: 'BASE_ITEM_NAME',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"c.NAME\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '订单号',\n" +
            "            key: 'CODE',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '客户单号',\n" +
            "            key: 'COSTOMER_CODE',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"a.COSTOMER_CODE\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '订单时间',\n" +
            "            key: 'TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '已收数量',\n" +
            "            key: 'RECEIPTS_NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求发货时间',\n" +
            "            key: 'ESTIMATE_DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '实际发货时间',\n" +
            "            key: 'DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求到货时间',\n" +
            "            key: 'ESTIMATE_ARRIVE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '实际到货时间',\n" +
            "            key: 'ARRIVE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '运输方式',\n" +
            "            key: 'TRANSPORT_TYPE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '运输性质',\n" +
            "            key: 'TRANSPORT_NATURE',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '配载方式',\n" +
            "            key: 'LTL',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '收货单位',\n" +
            "            key: 'SNAME',\n" +
            "            width: 180,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货人',\n" +
            "            key: 'SPERSION',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货电话',\n" +
            "            key: 'SPHONE',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货地址',\n" +
            "            key: 'SADDRESS',\n" +
            "            width: 230,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            queryIndex:\"a.DESCRIPTION\",\n" +
            "          },\n" +
            "        ]";
    String SHIPMENT_LOG=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发车批次',\n" +
            "            key: 'CODE',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.CODE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '关联单号',\n" +
            "            key: 'TMS_ORDER_CODE',\n" +
            "            width: 200,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"b.CODE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发运时间',\n" +
            "            key: 'DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '车牌号',\n" +
            "            key: 'LICENSE_NUMBER',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.LICENSE_NUMBER\"\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '司机',\n" +
            "            key: 'DRIVER',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.DRIVER\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '司机电话',\n" +
            "            key: 'DRIVER_PHONE',\n" +
            "            width: 120,\n" +
            "            query:\"text\",\n" +
            "            queryIndex:\"a.DRIVER_PHONE\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '跟踪方式',\n" +
            "            key: 'TRACK_MODEL',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '运输方式',\n" +
            "            key: 'TRANSPORT_TYPE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            queryIndex:\"a.DESCRIPTION\",\n" +
            "          },]";
    String SHIPMENT_LOG_ORDER = " [\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '订单号',\n" +
            "            key: 'CODE',\n" +
            "            width: 150,\n" +
            "            queryIndex:\"a.CODE\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '提货',\n" +
            "            key: 'TASK',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '送货',\n" +
            "            key: 'DELIVER',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '客户单号',\n" +
            "            key: 'COSTOMER_CODE',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"a.COSTOMER_CODE\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发车批次',\n" +
            "            key: 'TMS_SHIPMENT_CODE',\n" +
            "            width: 120,\n" +
            "            queryIndex:\"e.CODE\",\n" +
            "          },\n" +
            "          {\n" +
            "            title: '订单时间',\n" +
            "            key: 'TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '跟踪方式',\n" +
            "            key: 'TRACK_MODEL',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求发货时间',\n" +
            "            key: 'ESTIMATE_DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '实际发货时间',\n" +
            "            key: 'DESPATCH_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '要求到货时间',\n" +
            "            key: 'ESTIMATE_ARRIVE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '实际到货时间',\n" +
            "            key: 'ARRIVE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '运输方式',\n" +
            "            key: 'TRANSPORT_TYPE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '配载方式',\n" +
            "            key: 'LTL',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货单位',\n" +
            "            key: 'SNAME',\n" +
            "            width: 180,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货人',\n" +
            "            key: 'SPERSION',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货电话',\n" +
            "            key: 'SPHONE',\n" +
            "            width: 120,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货地址',\n" +
            "            key: 'SADDRESS',\n" +
            "            width: 230,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '发货数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '收货数量',\n" +
            "            key: 'REFUSE_NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '未收数量',\n" +
            "            key: 'UNCOLLECTED_NUMBER',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '总货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 90,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            width: 350,\n" +
            "          },\n" +
            "        ]";
    String SHIPMENT_LOG_PRODUCTS = " [\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货品代码',\n" +
            "            key: 'CODE',\n" +
            "            width: 180,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货品名称',\n" +
            "            key: 'NAME',\n" +
            "            align: 'center',\n" +
            "            width: 160,\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '单位',\n" +
            "            key: 'UNIT',\n" +
            "            align: 'center',\n" +
            "            width: 90,\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '数量',\n" +
            "            key: 'NUMBER',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '毛重',\n" +
            "            key: 'ROUGT_WEIGHT',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '净重',\n" +
            "            key: 'NET_WEIGHT',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '体积',\n" +
            "            key: 'VOLUME',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '货值',\n" +
            "            key: 'VALUE',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '清空',\n" +
            "            // slot: 'action',\n" +
            "            width: 80,\n" +
            "            align: 'center',\n" +
            "\n" +
            "          },\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            width: 110,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "        ]";
    String BASE_VEHICLE=" [\n" +
            "          {\n" +
            "            title:'选择',\n" +
            "            type: 'selection',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "            key: 'ID',\n" +
            "          },\n" +
            "          {\n" +
            "            title:'序号',\n" +
            "            type: 'index',\n" +
            "            width: 60,\n" +
            "            align: 'center',\n" +
            "          },\n" +
            "          {\n" +
            "            title: '车牌号',\n" +
            "            key: 'CODE',\n" +
            "            width: 180,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '类型',\n" +
            "            key: 'TYPE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '车型',\n" +
            "            key: 'VEHICLE_TYPE',\n" +
            "            width: 130,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '司机',\n" +
            "            key: 'DEIVER_NAME',\n" +
            "            width: 100,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '司机电话',\n" +
            "            key: 'DEIVER_PHONE',\n" +
            "            width: 100,\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "          {\n" +
            "            title: '状态',\n" +
            "            key: 'STATE',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: 'GPS品牌',\n" +
            "            key: 'GPS_SUPPLIER',\n" +
            "            width: 90,\n" +
            "          },\n" +
            "          {\n" +
            "            title: 'GPS号',\n" +
            "            key: 'GPS_CODE',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立人',\n" +
            "            key: 'CREATE_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '建立时间',\n" +
            "            key: 'CREATE_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改人',\n" +
            "            key: 'ALERT_NAME',\n" +
            "            width: 100,\n" +
            "          },\n" +
            "          {\n" +
            "            title: '修改时间',\n" +
            "            key: 'ALERT_TIME',\n" +
            "            width: 150,\n" +
            "          },\n" +
            "\n" +
            "          {\n" +
            "            title: '备注',\n" +
            "            key: 'DESCRIPTION',\n" +
            "            query:\"text\"\n" +
            "          },\n" +
            "        ]";
}
