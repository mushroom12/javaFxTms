package com.zym.application.controller.base.kpi;

import com.alibaba.fastjson.JSONArray;
import com.zym.application.service.BaseDataSetvice;
import com.zym.framework.area.Area;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymField;
import com.zym.framework.model.SelectModel;
import com.zym.framework.textfield.NumberTextField;
import com.zym.framework.utlis.FromUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseKpiFromController implements Initializable {
    @FXML
    @ZymField
    public ZymComboBox BASE_ITEM_ID;
    @FXML
    @ZymField
    public Area FAREA;
    @FXML
    @ZymField
    public Area SAREA;
    @FXML
    @ZymField
    public ZymComboBox COUNT_TYPE;
    @FXML
    @ZymField
    public NumberTextField DESPATCH_LENGTH;
    @FXML
    @ZymField
    public NumberTextField ARRIVE_LENGTH;
    @FXML
    @ZymField
    private TextField DESCRIPTION;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        BaseDataSetvice instance = BaseDataSetvice.getInstance();
        JSONArray arrayData = instance.getArrayData("/item/init");
        List<SelectModel> list = BaseDataSetvice.toSelectModelList(arrayData, "ID", "NAME");
        BASE_ITEM_ID.getItems().setAll(list);
        FromUtils.initDataBook("KpiType",COUNT_TYPE);
    }
}
