package com.zym.application.controller.base.costmanage;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.config.SystemConfig;
import com.zym.application.service.BaseDataSetvice;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymField;
import com.zym.framework.model.SelectModel;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseCostManageFromController implements Initializable {
    @FXML
    @ZymField
    public TextField CLASSIFIED_SUBJECT;
    @FXML
    @ZymField
    public ZymComboBox TYPE;
    @FXML
    @ZymField
    public ZymComboBox TAXTATE;
    @FXML
    @ZymField
    private TextField NAME;
    @FXML
    @ZymField
    private TextField CODE;
    @FXML
    @ZymField
    private TextField DESCRIPTION;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        JSONObject dataBook = SystemConfig.getInstance().getDataBook();
        JSONObject fylx = dataBook.getJSONObject("FYLX");
        for (Map.Entry<String, Object> m : fylx.entrySet()) {
            SelectModel selectModel = new SelectModel();
            selectModel.setId(m.getKey());
            selectModel.setValue(m.getValue().toString());
            TYPE.getItems().add(selectModel);
        }
        TYPE.initValue();
        BaseDataSetvice baseDataSetvice = BaseDataSetvice.getInstance();
        JSONArray arrayData = baseDataSetvice.getArrayData("/costManage/init");
        List list = baseDataSetvice.toSelectModelList(arrayData, "TAX_TATE", "TAX_TATE");
        TAXTATE.getItems().setAll(list);
    }
}
