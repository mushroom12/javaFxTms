package com.zym.application.controller.base.customerSupplier;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.config.SystemConfig;
import com.zym.application.service.BaseDataSetvice;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymField;
import com.zym.framework.model.SelectModel;
import com.zym.framework.textfield.NumberTextField;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseCSFromController  implements Initializable {
    @FXML
    @ZymField
    public TextField SELL_PERSION;
    @FXML
    @ZymField
    public TextField BUSINESS_PERSION;
    @FXML
    @ZymField
    public TextField BUSINESS_PHONE;
    @FXML
    @ZymField
    public TextField BUSINESS_EMAIL;
    @FXML
    @ZymField
    public TextField REGISTRATION;
    @FXML
    @ZymField
    public TextField INVOICE_TITLE;
    @FXML
    @ZymField
    public TextField INVOICE_ADDRESS;
    @FXML
    @ZymField
    public TextField INVOICE_PHONE;
    @FXML
    @ZymField
    public TextField BANK_OF_DEPOSIT;
    @FXML
    @ZymField
    public TextField BANK_ACCOUNT;
    @FXML
    @ZymField
    public NumberTextField CHECK_ACCONUNTS_PERIOD_MONTH;
    @FXML
    @ZymField
    public ZymComboBox CHECK_ACCONUNTS_PERIOD_DAY;
    @FXML
    @ZymField
    public ZymComboBox SETTLE_ACCOUNTS_PERIOD_DAY;
    @FXML
    @ZymField
    public NumberTextField SETTLE_ACCOUNTS_PERIOD_MONTH;
    @FXML
    @ZymField
    private TextField NAME;
    @FXML
    @ZymField
    private TextField CODE;
    @FXML
    @ZymField
    private TextField BUSINESS_ADDRESS;
    @FXML
    @ZymField
    private TextField ABBERVIATION;
    @FXML
    @ZymField
    private CheckBox CUSTOMER;
    @FXML
    @ZymField
    private CheckBox SUPPLIER;
    @FXML
    @ZymField
    private ZymComboBox SETTLE_ACCOUNTS_TYPE;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //初始化
        CUSTOMER.setSelected(true);
        JSONObject dataBook = SystemConfig.getInstance().getDataBook();
        ObservableList<SelectModel> items = SETTLE_ACCOUNTS_TYPE.getItems();
        JSONObject settleAccountsType = dataBook.getJSONObject("SettleAccountsType");
        for (Map.Entry<String, Object> m : settleAccountsType.entrySet()) {
            SelectModel selectModel = new SelectModel();
            selectModel.setId(m.getKey());
            selectModel.setValue(m.getValue().toString());
            items.add(selectModel);
        }
        SETTLE_ACCOUNTS_TYPE.initValue();
        //天
        JSONObject day = dataBook.getJSONObject("Day");
        for (Map.Entry<String, Object> m : day.entrySet()) {
            SelectModel selectModel = new SelectModel();
            selectModel.setId(m.getKey());
            selectModel.setValue(m.getValue().toString());
            SETTLE_ACCOUNTS_PERIOD_DAY.getItems().add(selectModel);
            CHECK_ACCONUNTS_PERIOD_DAY.getItems().add(selectModel.clone());
        }

    }
}
