package com.zym.application.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.config.SystemConfig;
import com.zym.application.model.ReturnModel;
import com.zym.application.service.PageService;
import com.zym.application.session.Http;
import com.zym.application.utils.DateUtils;
import com.zym.application.utils.LogUtils;
import com.zym.application.utils.StringUtils;
import com.zym.application.utils.SystemUtils;
import com.zym.framework.constants.color;
import com.zym.framework.notice.NoticeUtils;
import com.zym.framework.progress.ProgressTask;
import com.zym.framework.progress.ProgressUtils;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-01-29
 */
public class LoginController  implements Initializable {


    private SystemConfig config = SystemConfig.getInstance();
    @FXML
    private Button out;
    @FXML
    private TextField code;
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private CheckBox recallPassword;
    @FXML
    private AnchorPane vf;
    public void systemOut(ActionEvent event){
        SystemUtils.systemOut();
    }

    public void initialize(URL location, ResourceBundle resources) {
        String defaultOrgId = config.getDefaultOrgId();
        if(StringUtils.isNotEmpty(defaultOrgId)){
            code.setText(defaultOrgId);
        }
        String defaultUserName = config.getDefaultUserName();
        if(StringUtils.isNotEmpty(defaultUserName)){
            username.setText(defaultUserName);
        }
        if(config.getXmlRecallPassword()){
            recallPassword.setSelected(true);
            password.setText(config.getDefaultPassword());
        }
    }

    public void login(){
        ObservableList<Node> children = vf.getChildren();
        children.remove(10,children.size());
        username.setStyle("");
        password.setStyle("");
        code.setStyle("");
        String ps = password.getText();
        String un = username.getText();
        String cd = code.getText();
        int errorNumber = 0;
        if(StringUtils.isEmpty(ps)){
            errorNumber++;
            password.setStyle("-fx-border-color: "+ color.ERROR_COLOR+"; -fx-border-width: 1.2;");
            Label label = new Label();
            label.setId("ep");
            label.setText("密码不能为空");
            label.setStyle("-fx-text-fill: "+ color.ERROR_COLOR+";");
            label.setLayoutX(password.getLayoutX()+password.getWidth()+13);
            label.setLayoutY(password.getLayoutY()+7);
            children.add(label);
        }
        if(StringUtils.isEmpty(un)){
            errorNumber++;
            username.setStyle("-fx-border-color: "+ color.ERROR_COLOR+"; -fx-border-width: 1.2;");
            Label label = new Label();
            label.setId("eu");
            label.setText("账户不能为空");
            label.setStyle("-fx-text-fill: "+ color.ERROR_COLOR+";");
            label.setLayoutX(username.getLayoutX()+username.getWidth()+13);
            label.setLayoutY(username.getLayoutY()+7);
            children.add(label);
        }
        if(StringUtils.isEmpty(cd)){
            errorNumber++;
            code.setStyle("-fx-border-color: "+ color.ERROR_COLOR+"; -fx-border-width: 1.2;");
            Label label = new Label();
            label.setId("ec");
            label.setText("公司代码不能为空");
            label.setStyle("-fx-text-fill: "+ color.ERROR_COLOR+";");
            label.setLayoutX(code.getLayoutX()+code.getWidth()+13);
            label.setLayoutY(code.getLayoutY()+7);
            children.add(label);
        }
        if(errorNumber > 0){
            //验证信息
            return;
        }
        Map requestData =  new HashMap();
        requestData.put("PHONE",un);
        requestData.put("PASSWORD",ps);
        requestData.put("DEVICE","pc");
        requestData.put("CODE",cd);
        Map map = new HashMap();
        map.put("jsonBean",requestData);
        boolean isLogin = false;
        ReturnModel post = Http.postNotProgress("/login/login", map);
        if(post.isSuceess()){
            ProgressTask<Void> progressTask = new ProgressTask() {
                @Override
                protected Void execute() throws Exception {
                    updateInfo(25,100,"验证通过...");
                    LogUtils.println("登录成功...");
                    //获取系统配置
                    config.setLoginData(JSONObject.parseObject(post.getBean().toString()));
                    updateInfo(35,100,"加载菜单...");
                    LogUtils.println("加载菜单...");
                    JSONArray menuData = config.getMenuData();
                    PageService.getInstance().loadMenu(menuData);

                    JSONObject systemConfig = Http.postJSONObject("/config/getSystemConfig", null);
                    config.setSystemData(systemConfig);
                    //获取数据字典
                    LogUtils.println("获取系统配置...");

                    updateInfo(55,100,"获取数据字典...");
                    JSONObject orgConfig = Http.postJSONObject("/tmssystem/getSystemConfig", null);
                    //获取组织机构数据
                    config.setOrgConfig(orgConfig);
                    updateInfo(75,100,"获取系统配置...");
                    JSONObject dataBook = Http.postJSONObject("/databook/getDataBook", null);
                    config.setDataBook(dataBook);
                    updateInfo(95,100,"更新关键配置...");
                    LogUtils.println("更新XML...");
                    boolean selected = recallPassword.isSelected();
                    config.setXmlRecallPassword(selected);
                    if(selected){
                        config.setXmlPassword(ps);
                    }
                    config.setXmlUserInfo(cd,un);
                    config.updateXml();
                    updateInfo(100,100,"完成...");
                    //多线程内跳转会报错。因为此线程调用主线窗口管理会产生冲突
                    return null;
                }
            };

            ProgressUtils sb = ProgressUtils.create( code.getScene().getWindow(), progressTask, "测试 ");
            sb.showAndWait();
            //跳转主页面
            if(post.isSuceess()){
                String dateTime = DateUtils.formatNow("yyyy年MM月dd日 HH:mm:ss");
                String userName = config.getUserName();
                PageService.getInstance().goIndex("用户:"+userName+"  登录时间:"+dateTime);
            }

        }
    }
    public void test(){
        NoticeUtils.success("执行成功");
//        Alert alert = new Alert(Alert.AlertType.ERROR);
//        alert.setContentText("!23");
//        alert.show();
//        alert.close();

    }
}
