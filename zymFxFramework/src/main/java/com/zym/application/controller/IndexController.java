package com.zym.application.controller;

import com.zym.application.service.PageService;
import com.zym.application.utils.LogUtils;
import com.zym.application.utils.SystemUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class IndexController  implements Initializable {
    @FXML
    private MenuBar menuBar;
    @FXML
    private TabPane tabPane;
    @FXML
    private BorderPane bp;
    public void systemOut(){
        SystemUtils.systemOut();
    }
    public void goLogin(){
        PageService.getInstance().goLogin();
    }
    public void goIndex(){
        LogUtils.println("跳转主页");
        bp.setCenter(tabPane);
    }
    public void goGaode(){
        LogUtils.println("跳转高德");
        PageService instance = PageService.getInstance();
        bp.setCenter(instance.getWebView());
        instance.goWeb("https://www.amap.com");
    }
    public void goTianqi(){
        LogUtils.println("跳转天气窗口");
        PageService instance = PageService.getInstance();
        bp.setCenter(instance.getWebView());
        //这个网站可能多次报错 不爱解决，不影响使用
        instance.goWeb("http://www.weather.com.cn");
    }
    public void goOil(){
        LogUtils.println("跳转油价查询");
        PageService instance = PageService.getInstance();
        bp.setCenter(instance.getWebView());
        instance.goWeb("http://youjia.chemcp.com");
    }
    public void goBs(){
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+ "http://www.iasyn.cn/login");
        } catch (IOException e) {
            e.printStackTrace();
        }
//        LogUtils.println("跳转BS查询");
//        PageService instance = PageService.getInstance();
//        bp.setCenter(instance.getWebView());
//        instance.goWeb("http://www.iasyn.cn/login");
    }
    public MenuBar getMenuBar(){
        return menuBar;
    }

    public TabPane getTabPane() {
        return tabPane;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    public void test(){
        System.out.println(123);
    }
}
