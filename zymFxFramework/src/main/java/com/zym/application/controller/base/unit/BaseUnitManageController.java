package com.zym.application.controller.base.unit;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.application.utils.StringUtils;
import com.zym.framework.alert.AlertUtils;
import com.zym.framework.form.ZymCallBack;
import com.zym.framework.form.ZymForm;
import com.zym.framework.grid.ZymGrid;
import com.zym.framework.notice.NoticeUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class BaseUnitManageController implements Initializable {
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
    private String fromXmlPath = "/view/base/unitManage/base-unit-manage-from.fxml";
    private ZymForm zymForm;
    @FXML
    private TextField itemName;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        Map databook = new HashMap();
        zymGrid.initView("/unitManage/list",BaseData.BASE_UNIT,databook);
        zymGrid.setWinHeight(700);

        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);
        this.zymForm = new ZymForm("/unitManage/save","/unitManage/get",fromXmlPath,"单位管理");
        zymForm.setFormCallback(new ZymCallBack() {
            @Override
            public void submitEnd() {
                queryList();
            }
        });
        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        String id = jsonObject.getString("ID");
                        zymForm.open(id);
                    }
                });
                return row ;
            }
        });


    }
    public void queryList(){
        Map map = null;
        if(StringUtils.isNotEmpty(itemName.getText())){
            map = new HashMap();
            map.put("sqlWhereNameToLike","NAME");
            map.put("sqlWhereValueToLike",itemName.getText());
        }
        zymGrid.query(map);
    }
    public void open(){
        zymForm.open(null);
    }

    public void del(){
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() == 0){
            NoticeUtils.error("请勾选要更改的数据");
            return;
        }
        String str = "条数据执行删除？";
        if(!AlertUtils.confirmYesNoConfirmation("通知","您确定要对这"+ids.size()+str)){
            return;
        }
        JSONObject par = new JSONObject();
        par.put("ids",ids);
        Http.post("/unitManage/delete", par, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                if (returnModel.isSuceess()){
                    NoticeUtils.success(returnModel.getObj().toString());
                    queryList();
                }else {
                    NoticeUtils.error(returnModel.getObj().toString());
                }

            }
        });
    }
}
