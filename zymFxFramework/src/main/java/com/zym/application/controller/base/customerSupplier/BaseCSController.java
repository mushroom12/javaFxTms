package com.zym.application.controller.base.customerSupplier;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.application.utils.StringUtils;
import com.zym.framework.alert.AlertUtils;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.FormCallback;
import com.zym.framework.form.ZymForm;
import com.zym.framework.grid.ZymGrid;
import com.zym.framework.notice.NoticeUtils;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.net.URL;
import java.util.*;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class BaseCSController implements Initializable {
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
    private String fromXmlPath = "/view/base/customerSupplier/base-customer-supplier-from.fxml";
    private ZymForm zymForm;
    @FXML
    private TextField itemName;
    @FXML
    private ZymComboBox select;
    @FXML
    private CheckBox isC;
    @FXML
    private CheckBox isS;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        Map databook = new HashMap();
        databook.put("STATE","CommState");
        databook.put("SUPPLIER","Istrue");
        databook.put("CUSTOMER","Istrue");
        databook.put("CHECK_ACCONUNTS_PERIOD", new Callback<TableColumn.CellDataFeatures<Map, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map, String> param) {
                Map value = param.getValue();
                Object ck = value.get("CHECK_ACCONUNTS_PERIOD");
                if(ck != null){
                    String check_acconunts_period = ck.toString();
                    String[] sp = check_acconunts_period.split(",");
                    if(sp.length == 1){
                        return new ReadOnlyObjectWrapper(sp[0]+"个月后");
                    }else {
                        return new ReadOnlyObjectWrapper(sp[0]+"个月后 "+sp[1]+"号");
                    }
                }
               return null;
            }
        });
        databook.put("SETTLE_ACCOUNTS_PERIOD", new Callback<TableColumn.CellDataFeatures<Map, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Map, String> param) {
                Map value = param.getValue();
                Object ck = value.get("SETTLE_ACCOUNTS_PERIOD");
                if(ck != null){
                    String check_acconunts_period = ck.toString();
                    String[] sp = check_acconunts_period.split(",");
                    if(sp.length == 1){
                        return new ReadOnlyObjectWrapper(sp[0]+"个月后");
                    }else {
                        return new ReadOnlyObjectWrapper(sp[0]+"个月后 "+sp[1]+"号");
                    }
                }
                return null;
            }
        });
        zymGrid.initView("/basecustomersupplier/listToOrg",BaseData.BaseCustomerSupplier,databook);
        zymGrid.setWinHeight(700);

        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);
        this.zymForm = new ZymForm("/basecustomersupplier/saveCS","/basecustomersupplier/get",fromXmlPath,"客户管理");
        zymForm.setFormCallback(new FormCallback() {
            @Override
            public void submitEnd() {
                queryList();
            }
            @Override
            public void submitStart(JSONObject jsonBean) {
                JSONObject jsonObject = jsonBean.getJSONObject("jsonBean");
                if(jsonObject.getString("CHECK_ACCONUNTS_PERIOD_MONTH") != null && jsonObject.getString("CHECK_ACCONUNTS_PERIOD_DAY")!=null){
                    jsonObject.put("CHECK_ACCONUNTS_PERIOD",jsonObject.getString("CHECK_ACCONUNTS_PERIOD_MONTH")+","+jsonObject.getString("CHECK_ACCONUNTS_PERIOD_DAY"));
                }
                if(jsonObject.getString("SETTLE_ACCOUNTS_PERIOD_MONTH") != null && jsonObject.getString("SETTLE_ACCOUNTS_PERIOD_DAY")!=null){
                    jsonObject.put("SETTLE_ACCOUNTS_PERIOD",jsonObject.getString("SETTLE_ACCOUNTS_PERIOD_MONTH")+","+jsonObject.getString("SETTLE_ACCOUNTS_PERIOD_DAY"));
                }
                jsonObject.remove("CHECK_ACCONUNTS_PERIOD_MONTH");
                jsonObject.remove("CHECK_ACCONUNTS_PERIOD_DAY");
                jsonObject.remove("SETTLE_ACCOUNTS_PERIOD_MONTH");
                jsonObject.remove("SETTLE_ACCOUNTS_PERIOD_DAY");
                System.out.println(jsonObject);
            }

            @Override
            public void getStart(Map map) {
                JSONObject data = new JSONObject(map);
                if( data.getString("CHECK_ACCONUNTS_PERIOD")!= null){
                    String[] check_acconunts_periods = data.getString("CHECK_ACCONUNTS_PERIOD").split(",");
                    data.put("CHECK_ACCONUNTS_PERIOD_MONTH",check_acconunts_periods[0]);
                    if(check_acconunts_periods.length ==2){
                        data.put("CHECK_ACCONUNTS_PERIOD_DAY",check_acconunts_periods[1]);
                    }

                }
                if( data.getString("SETTLE_ACCOUNTS_PERIOD")!= null){
                    String[] settle_accounts_periods = data.getString("SETTLE_ACCOUNTS_PERIOD").split(",");
                    data.put("SETTLE_ACCOUNTS_PERIOD_MONTH",settle_accounts_periods[0]);
                    if(settle_accounts_periods.length ==2){
                        data.put("SETTLE_ACCOUNTS_PERIOD_DAY",settle_accounts_periods[1]);
                    }
                }
                data.remove("CHECK_ACCONUNTS_PERIOD");
                data.remove("SETTLE_ACCOUNTS_PERIOD");
                map = data;
            }
        });
        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        String id = jsonObject.getString("ID");
                        zymForm.open(id);
                    }
                });
                return row ;
            }
        });
        select.getItems().setAll( zymGrid.getSelectModels());
        select.initValue();
        isC.setSelected(true);

//        items.add(selectModel);

    }
    public void queryList(){
        Map map= new HashMap();
        if(StringUtils.isNotEmpty(itemName.getText())){
            map.put("sqlWhereNameToLike",select.getValue().getId());
            map.put("sqlWhereValueToLike",itemName.getText());
        }
        List<String> list = new ArrayList<>();
        list.add("0");
        list.add("1");
        list.add("2");
        if(isC.isSelected()){
            map.put("age1",1);
        }else {
            map.put("age1",0);
        }
        if(isS.isSelected()){
            map.put("age2",1);
        }else {
            map.put("age2",0);
        }
        map.put("ids",list);
        zymGrid.query(map);
    }
    public void open(){
        zymForm.open(null);
    }

    public void audit(){
        updateState(2);
    }
    public void unAudit(){

        updateState(0);
    }

    private void updateState(int state){
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() == 0){
            NoticeUtils.error("请勾选要更改的数据");
            return;
        }
        String str = "条数据审核？";
        if(state == 0){
            str= "条数据失效？";
        }
        if(!AlertUtils.confirmYesNoConfirmation("通知","您确定要对这"+ids.size()+str)){
            return;
        }
        JSONObject par = new JSONObject();
        par.put("ids",ids);
        par.put("name",state);
        Http.post("/basecustomersupplier/updateState", par, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                if (returnModel.isSuceess()){
                    NoticeUtils.success(returnModel.getObj().toString());
                    queryList();
                }else {
                    NoticeUtils.error(returnModel.getObj().toString());
                }

            }
        });
    }
}
