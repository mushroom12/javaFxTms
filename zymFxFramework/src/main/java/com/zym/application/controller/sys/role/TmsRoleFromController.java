package com.zym.application.controller.sys.role;

import com.zym.framework.form.ZymField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class TmsRoleFromController implements Initializable {
    @FXML @ZymField
    public TextField NAME;
    @FXML @ZymField
    public TextField CODE;
    @FXML @ZymField
    public TextField DESCRIPTION;



    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
