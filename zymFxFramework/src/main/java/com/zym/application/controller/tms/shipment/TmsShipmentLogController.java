package com.zym.application.controller.tms.shipment;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.utils.StringUtils;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.grid.ZymGrid;
import com.zym.framework.utlis.FxmlUtil;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class TmsShipmentLogController implements Initializable {
    @FXML
    public ZymComboBox select;
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
    private String fromXmlPath = "/view/tms/shipment/tms-shipment-log-from.fxml";
    @FXML
    private TextField itemName;
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        Map databook = new HashMap();
        databook.put("TRANSPORT_TYPE","TransportType");
        databook.put("TRACK_MODEL","TrackModel");
        zymGrid.initView("/tmsshipment/listShipmentLog",BaseData.SHIPMENT_LOG,databook);
        zymGrid.setWinHeight(700);

        FXMLLoader fxmlLoader = FxmlUtil.loadFxmlFromResource(fromXmlPath);
        TmsShipmentLogFromController controller = fxmlLoader.getController();


        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);
        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        String id = jsonObject.getString("ID");
                        controller.openFrom(id);
                    }
                });
                return row ;
            }
        });
        select.getItems().setAll( zymGrid.getSelectModels());
        select.initValue();

    }
    public void queryList(){
        Map map= new HashMap();
        if(StringUtils.isNotEmpty(itemName.getText())){
            map.put("sqlWhereNameToLike",select.getValue().getId());
            map.put("sqlWhereValueToLike",itemName.getText());
        }
        zymGrid.query(map);
    }


}
