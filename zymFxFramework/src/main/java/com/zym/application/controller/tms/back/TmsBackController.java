package com.zym.application.controller.tms.back;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.column.BaseData;
import com.zym.application.model.ReturnModel;
import com.zym.application.session.Http;
import com.zym.application.session.HttpCall;
import com.zym.application.utils.StringUtils;
import com.zym.framework.alert.AlertUtils;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymCallBack;
import com.zym.framework.form.ZymForm;
import com.zym.framework.grid.ZymGrid;
import com.zym.framework.notice.NoticeUtils;
import com.zym.framework.utlis.FromUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.net.URL;
import java.util.*;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-05
 */
public class TmsBackController implements Initializable {
    @FXML
    public ZymComboBox select;
    @FXML
    public ZymComboBox state;
    @FXML
    public DatePicker times1;
    @FXML
    public DatePicker times2;
    private FXMLLoader fxmlLoader;
    private ZymGrid zymGrid;
    @FXML
    private SplitPane splitPane;
    private String fromXmlPath = "/view/tms/back/tms-back-from.fxml";
    private ZymForm zymForm;
    @FXML
    private TextField itemName;
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.fxmlLoader = ZymGrid.create();
        this.zymGrid = fxmlLoader.getController();
        Map databook = new HashMap();
        databook.put("LTL","LTL");
        databook.put("TRANSPORT_NATURE","TransportNature");
        databook.put("TRANSPORT_TYPE","TransportType");
        databook.put("STATE","BackType");
        zymGrid.initView("/tmsorder/list",BaseData.BACK,databook);
        zymGrid.setWinHeight(700);
        Parent root = this.fxmlLoader.getRoot();
        splitPane.getItems().add(root);

        this.zymForm = new ZymForm("/tmsback/saveBack","/tmsback/getBackProductList",fromXmlPath,"回单管理"){
            @Override
            public void open(String id) {
                openBack(id);
            }
        };
        TmsBackFromController controller = this.zymForm.getController(TmsBackFromController.class);
        zymForm.setFormCallback(new ZymCallBack() {
            @Override
            public void submitEnd() {
                queryList();
            }
            @Override
            public void submitStart(JSONObject jsonObject) {
                List data = controller.getData();
                jsonObject.put("jsonBeans",data);
            }
        });
        zymGrid.setRowFactory(new Callback<TableView, TableRow>() {
            @Override
            public TableRow call(TableView param) {
                TableRow<JSONObject> row = new TableRow<JSONObject>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        JSONObject jsonObject = row.getItem();
                        zymForm.open(jsonObject.toJSONString());
                    }
                });
                return row ;
            }
        });
        FromUtils.initDataBook("BackType2",state);
        state.initValue();
        select.getItems().setAll( zymGrid.getSelectModels());
        select.initValue();
    }
    public void openBack(String id) {
        this.zymForm.reset();
        TmsBackFromController controller = zymForm.getController(TmsBackFromController.class);
        if(StringUtils.isNotEmpty(id)){
            JSONObject jsonObject = new JSONObject();
            JSONObject bean = JSONObject.parseObject(id);
            jsonObject.put("id",bean.getString("ID"));
            if( bean.getIntValue("STATE") == 6){ //锁定
                controller.lockFrom(true);
                zymForm.lockSaveButton(true);
            }else {
                controller.lockFrom(false);
                zymForm.lockSaveButton(false);
            }
            Http.post("/tmsback/getBackProductList", jsonObject, new HttpCall() {
                @Override
                public void exe(ReturnModel post) {
                    JSONArray data = (JSONArray) post.getObj();
                    //添加数据中
                    controller.addData(data);
                    controller.reset();
                    zymForm.setBean(bean);
                }
            });
        }
        this.zymForm.show();
    }


    public void queryList(){
        Map map= new HashMap();
        if(StringUtils.isNotEmpty(itemName.getText())){
            map.put("sqlWhereNameToLike",select.getValue().getId());
            map.put("sqlWhereValueToLike",itemName.getText());
        }
        List ids = new ArrayList();
        ids.add(state.getValue().getId());
        map.put("ids",ids);
        List time = new ArrayList();
        String text1 = times1.getEditor().getText();
        String text2 = times2.getEditor().getText();
        time.add(StringUtils.isNotEmpty(text1)?text1+"T16:00:00.000Z":null);
        time.add(StringUtils.isNotEmpty(text2)?text2+"T16:00:00.000Z":null);
        map.put("times",time);
        zymGrid.query(map);
    }
    public void open(){
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() != 1){
            NoticeUtils.error("回单只能单条操作");
            return;
        }
        zymForm.open(ids.get(0));
    }


    public void batchBack(ActionEvent actionEvent) {
        List<String>  ids= zymGrid.getSelectId();
        if(ids.size() == 0){
            NoticeUtils.error("请勾选要批量回单的数据");
            return;
        }
        String str = "条数据批量回单？";
        if(!AlertUtils.confirmYesNoConfirmation("通知","您确定要对这"+ids.size()+str)){
            return;
        }
        JSONObject par = new JSONObject();
        par.put("ids",ids);
        par.put("state",5);
        Http.post("'/tmsback/batchBack", par, new HttpCall() {
            @Override
            public void exe(ReturnModel returnModel) {
                if (returnModel.isSuceess()){
                    NoticeUtils.success(returnModel.getObj().toString());
                    queryList();
                }else {
                    NoticeUtils.error(returnModel.getObj().toString());
                }
            }
        });
    }
}
