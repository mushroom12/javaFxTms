package com.zym.application.controller.tms.back;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.constants.Grid;
import com.zym.framework.datetimepicker.DateTimePicker;
import com.zym.framework.form.ZymField;
import com.zym.framework.grid.IDCell;
import com.zym.framework.grid.JSONObjectCell;
import com.zym.framework.grid.edit.ZymNumberFieldTableFX;
import com.zym.framework.grid.edit.ZymTextFieldTableFX;
import com.zym.framework.model.SelectModel;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class TmsBackFromController implements Initializable {
    @FXML  @ZymField
    public DateTimePicker BACK_TIME;
    @FXML  @ZymField
    public TextArea APPRAISE_TEXT;
    @FXML
    public TableView table;
    @FXML  @ZymField
    public ZymComboBox APPRAISE_NUMBER;

    public void lockFrom(boolean is){
        APPRAISE_NUMBER.setDisable(is);
        BACK_TIME.setDisable(is);
        APPRAISE_TEXT.setDisable(is);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initGrid();
        for (int i = 1; i < 6; i++) {
            SelectModel selectModel = new SelectModel();
            selectModel.setId(String.valueOf(i));
            selectModel.setValue(i+"星");
            APPRAISE_NUMBER.getItems().add(selectModel);
        }
    }
    public void  reset(){
        APPRAISE_NUMBER.initValue(4);
    }
    private void initGrid() {
        ObservableList columns = table.getColumns();
        this.table.setFocusModel(null);
        TableColumn index = new TableColumn();
        index.setId(Grid.INDEX);
        index.setText("序号");
        index.setPrefWidth(50);
        index.setStyle("-fx-alignment: center;");
        index.setCellFactory(new IDCell<>());
        columns.add(index);

        TableColumn CODE = new TableColumn();
        CODE.setId("CODE");
        CODE.setText("货品代码");
        CODE.setPrefWidth(180);
        CODE.setCellValueFactory(new JSONObjectCell<String>("CODE"));
        columns.add(CODE);

        TableColumn NAME = new TableColumn();
        NAME.setId("NAME");
        NAME.setText("货品名称");
        NAME.setPrefWidth(180);
        NAME.setCellValueFactory(new JSONObjectCell<String>("NAME"));
        columns.add(NAME);

        TableColumn UNIT = new TableColumn();
        UNIT.setId("UNIT");
        UNIT.setText("单位");
        UNIT.setPrefWidth(90);
        UNIT.setCellValueFactory(new JSONObjectCell<String>("UNIT"));
        columns.add(UNIT);

        TableColumn NUMBER = new TableColumn();
        NUMBER.setId("NUMBER");
        NUMBER.setText("应收数量");
        NUMBER.setPrefWidth(90);
        NUMBER.setCellValueFactory(new JSONObjectCell<String>("NUMBER"));
        columns.add(NUMBER);

        TableColumn RECEIPTS_NUMBER = new TableColumn();
        RECEIPTS_NUMBER.setId("RECEIPTS_NUMBER");
        RECEIPTS_NUMBER.setText("实收数量");
        RECEIPTS_NUMBER.setPrefWidth(120);
        RECEIPTS_NUMBER.setEditable(true);
        RECEIPTS_NUMBER.setCellFactory(new ZymNumberFieldTableFX("RECEIPTS_NUMBER"));
        RECEIPTS_NUMBER.setCellValueFactory(new JSONObjectCell<String>("RECEIPTS_NUMBER"));
        columns.add(RECEIPTS_NUMBER);

        TableColumn RECEIPTS_DESCRIPTION = new TableColumn();
        RECEIPTS_DESCRIPTION.setId("RECEIPTS_DESCRIPTION");
        RECEIPTS_DESCRIPTION.setText("收货描述");
        RECEIPTS_DESCRIPTION.setPrefWidth(200);
        RECEIPTS_DESCRIPTION.setCellFactory(new ZymTextFieldTableFX("RECEIPTS_DESCRIPTION"));
        RECEIPTS_DESCRIPTION.setCellValueFactory(new JSONObjectCell<String>("RECEIPTS_DESCRIPTION"));
        columns.add(RECEIPTS_DESCRIPTION);
    }

    public void addData(JSONArray data) {
        for (int i = 0; i < data.size(); i++) {
            JSONObject jsonObject = data.getJSONObject(i);
            jsonObject.put("RECEIPTS_NUMBER",jsonObject.get("NUMBER"));
        }
        this.table.getItems().setAll(data);
    }
    public List getData(){
        return this.table.getItems();
    }

}
