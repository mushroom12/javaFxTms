package com.zym.application.controller.tms.order;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.config.SystemConfig;
import com.zym.application.service.BaseDataSetvice;
import com.zym.application.supplis.ProductScan;
import com.zym.application.utils.StringUtils;
import com.zym.framework.CallBack;
import com.zym.framework.area.Area;
import com.zym.framework.combobox.AddressAutoComplete;
import com.zym.framework.combobox.AutoComplete;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.constants.Grid;
import com.zym.framework.datetimepicker.DateTimePicker;
import com.zym.framework.form.ZymField;
import com.zym.framework.grid.IDCell;
import com.zym.framework.grid.JSONObjectCell;
import com.zym.framework.grid.edit.ZymNumberFieldTableFX;
import com.zym.framework.grid.edit.ZymSelectFx;
import com.zym.framework.grid.edit.ZymTextFieldTableFX;
import com.zym.framework.icon.Icon;
import com.zym.framework.textfield.NumberTextField;
import com.zym.framework.utlis.FromUtils;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.util.Callback;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class TmsOrderFromController implements Initializable {
    @FXML
    public ScrollPane productPane;
    @FXML @ZymField
    public TextField CODE;
    @FXML @ZymField
    public TextField DESCRIPTION;
    @FXML @ZymField
    public ZymComboBox TRACK_MODEL;
    @FXML @ZymField
    public CheckBox DELIVER;
    @FXML @ZymField
    public CheckBox TASK;
    @FXML @ZymField
    public ZymComboBox BASE_VEHICLE_TYPE;
    @FXML @ZymField
    public NumberTextField BACK_NUMBER;
    @FXML @ZymField
    public DateTimePicker ESTIMATE_ARRIVE_TIME;
    @FXML @ZymField
    public DateTimePicker ESTIMATE_DESPATCH_TIME;
    @FXML @ZymField
    public DateTimePicker TIME;
    @FXML @ZymField
    public TextField COSTOMER_CODE;
    @FXML @ZymField
    public ZymComboBox TRANSPORT_NATURE;
    @FXML @ZymField
    public ZymComboBox TRANSPORT_TYPE;
    @FXML @ZymField
    public ZymComboBox LTL;
    @FXML @ZymField
    public ZymComboBox BASE_ITEM_ID;
    @FXML
    public ScrollPane sLedgerPane;
    @FXML
    public ScrollPane cLedgerPane;
    @FXML @ZymField
    public TextField FNAME;
    @FXML @ZymField
    public TextField FPERSION;
    @FXML @ZymField
    public TextField FPHONE;
    @FXML @ZymField
    public Area FAREA;
    @FXML @ZymField
    public ComboBox FADDRESS;
    @FXML @ZymField
    public ComboBox SADDRESS;
    @FXML @ZymField
    public Area SAREA;
    @FXML @ZymField
    public TextField SPHONE;
    @FXML @ZymField
    public TextField SPERSION;
    @FXML @ZymField
    public TextField SNAME;
    @FXML
    public Label BASE_VEHICLE_TYPE_LABEL;
    @FXML
    public Label COSTOMER_CODE_LABEL;
    @FXML
    public Label SPHONE_LABEL;
    @FXML
    public Label ESTIMATE_ARRIVE_TIME_LABEL;
    @FXML
    public Label SNAME_LABEL;
    @FXML
    public Label SAREA_LABEL;
    @FXML
    public Label SADDRESS_LABEL;
    @FXML
    public Label FPERSION_LABEL;
    @FXML
    public Label FADDRESS_LABEL;
    @FXML
    public Label FPHONE_LABEL;
    @FXML
    public Label ESTIMATE_DESPATCH_TIME_LABEL;
    @FXML
    public Label FNAME_LABEL;
    @FXML
    public Label FAREA_LABEL;
    @FXML
    public Label SPERSION_LABEL;
    @FXML
    private TableView product;
    private TableView sledger;
    private TableView cledger;
    private JSONObject initData;
    private BaseDataSetvice instance = BaseDataSetvice.getInstance();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initData();
        FromUtils.initDataBookDefault("LTL",LTL);
        FromUtils.initDataBookDefault("TransportType",TRANSPORT_TYPE);
        FromUtils.initDataBookDefault("TransportNature",TRANSPORT_NATURE);
        FromUtils.initDataBookDefault("TrackModel",TRACK_MODEL);
        List list = BaseDataSetvice.toSelectModelList(initData.getJSONArray("BASE_ITEM"), "ID", "NAME");
        List list2 = BaseDataSetvice.toSelectModelList(initData.getJSONArray("BASE_VEHICLE_TYPE"), "CODE", "CODE");
        BASE_VEHICLE_TYPE.getItems().setAll(list2);
        BASE_ITEM_ID.getItems().setAll(list);
        new AddressAutoComplete(new AutoComplete<>(SADDRESS));
        new AddressAutoComplete(new AutoComplete<>(FADDRESS));
        //对FROM加工
        SystemConfig instance = SystemConfig.getInstance();
        JSONObject orgConfig = instance.getOrgConfig();
        initFrom(orgConfig);
        initPorduct();
        initSledger();
        initCledger();
    }

    private void initFrom(JSONObject orgConfig) {
        Paint paint = Paint.valueOf("#ed321f");
        if(orgConfig.getIntValue("BASE_VEHICLE_TYPE") ==1){
            BASE_VEHICLE_TYPE_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("COSTOMER_CODE") ==1){
            COSTOMER_CODE_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("SPHONE") ==1){
            SPHONE_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("ESTIMATE_ARRIVE_TIME") ==1){
            ESTIMATE_ARRIVE_TIME_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("SNAME") ==1){
            SNAME_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("SAREA") ==1){
            SAREA_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("SADDRESS") ==1){
            SADDRESS_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("FPERSION") ==1){
            FPERSION_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("FADDRESS") ==1){
            FADDRESS_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("FPHONE") ==1){
            FPHONE_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("ESTIMATE_DESPATCH_TIME") ==1){
            ESTIMATE_DESPATCH_TIME_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("FNAME") ==1){
            FNAME_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("FAREA") ==1){
            FAREA_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("SPERSION") ==1){
            SPERSION_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("COSTOMER_CODE") ==1){
            COSTOMER_CODE_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("COSTOMER_CODE") ==1){
            COSTOMER_CODE_LABEL.setTextFill(paint);
        }
        if(orgConfig.getIntValue("COSTOMER_CODE") ==1){
            COSTOMER_CODE_LABEL.setTextFill(paint);
        }









    }

    public List getProductItems(){
        return  this.product.getItems();
    }
    public List getSledgerItems(){
        return  this.sledger.getItems();
    }
    public List getCledgerItems(){
        return  this.cledger.getItems();
    }
    public void setProductItems(List list){
       this.product.getItems().setAll(list);
    }
    public void setSledgerItems(List list){
       this.sledger.getItems().setAll(list);
    }
    public void setCledgerItems(List list){
       this.cledger.getItems().setAll(list);
    }


    public void reset(){
        this.cledger.getItems().clear();
        this.sledger.getItems().clear();
        this.product.getItems().clear();
        LTL.initValue();
        TRANSPORT_TYPE.initValue();
        TRANSPORT_NATURE.initValue();
        TRACK_MODEL.initValue();
        BASE_ITEM_ID.initValue();
        ESTIMATE_DESPATCH_TIME.dateTimeProperty().setValue(null);
        ESTIMATE_ARRIVE_TIME.dateTimeProperty().setValue(null);
    }
    private Button cledgerAdd;
    private void initCledger() {
        this.cledger = new TableView();
        this.cledger.setFocusModel(null);
        ObservableList columns = cledger.getColumns();
        this.cledger.setPrefHeight(305);
        TableColumn index = new TableColumn();
        index.setId(Grid.INDEX);
        index.setText("序号");
        index.setPrefWidth(50);
        index.setStyle("-fx-alignment: center;");
        index.setCellFactory(new IDCell<>());
        columns.add(index);

        //new 搜索框


        TableColumn ADD = new TableColumn();
        ADD.setId("ADD");
        ADD.setPrefWidth(60);
        ADD.setSortable(false);
        cledgerAdd = new Button();
        cledgerAdd.setDisable(!this.cledger.isEditable());
        ImageView imageView = new ImageView(Icon.ADD);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        cledgerAdd.setGraphic(imageView);
        cledgerAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ObservableList items = cledger.getItems();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BASE_COST_MANAGE_ID",null);
                jsonObject.put("AMOUNT","0.0");
                jsonObject.put("TAXRATE","0.0");
                jsonObject.put("INPUT","0.0");
                items.add(jsonObject);
            }
        });
        ADD.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn param) {
                TableCell<Map<String, String>, Object> cell = new TableCell<Map<String, String>, Object>() {
                    protected  void   updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            this.setGraphic(null);
                        }else {
                            Button button1 = new Button();
                            button1.setDisable(!cledger.isEditable());
                            ImageView imageView = new ImageView(Icon.CENTEL);
                            imageView.setFitWidth(20);
                            imageView.setFitHeight(20);
                            button1.setGraphic(imageView);
                            button1.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    ObservableList items = cledger.getItems();
                                    items.remove(getIndex());
                                }
                            });
                            this.setGraphic(button1);
                        }
                    }

                };
                cell.setId("DEL");
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        ADD.setGraphic(cledgerAdd);
        columns.add(ADD);


        TableColumn CODE = new TableColumn();
        CODE.setId("BASE_COST_MANAGE_ID");
        CODE.setText("费用类型");
        CODE.setEditable(true);
        CODE.setPrefWidth(230);
        List list = BaseDataSetvice.toSelectModelList(initData.getJSONArray("BASE_COST_MANAGE_C"), "ID", "NAME");
        CODE.setCellFactory(new ZymSelectFx("BASE_COST_MANAGE_ID",list));////表格编辑 但是得双击 不好~
        CODE.setCellValueFactory(new JSONObjectCell<String>("BASE_COST_MANAGE_ID"));

        columns.add(CODE);
        TableColumn AMOUNT = new TableColumn();
        AMOUNT.setId("AMOUNT");
        AMOUNT.setText("金额");
        AMOUNT.setPrefWidth(120);
        AMOUNT.setEditable(true);
        AMOUNT.setCellFactory(new ZymNumberFieldTableFX("AMOUNT"));
        AMOUNT.setCellValueFactory(new JSONObjectCell<String>("AMOUNT"));
        columns.add(AMOUNT);

        TableColumn INPUT= new TableColumn();


        TableColumn TAXRATE = new TableColumn();
        TAXRATE.setId("TAXRATE");
        TAXRATE.setText("税率");
        TAXRATE.setPrefWidth(120);
        TAXRATE.setEditable(true);
        JSONArray baseTaxrate = initData.getJSONArray("BASE_TAXRATE");
        List list2 = BaseDataSetvice.toSelectModelList(baseTaxrate, "TAX_TATE", "TAX_TATE");
        ZymSelectFx taxrate = new ZymSelectFx("TAXRATE", list2);
//        this.sledger.getItems().addListener(new ListChangeListener() {
//            @Override
//            public void onChanged(Change c) {
//                System.out.println(c);
//            }
//        });
//        taxrate.setChangeListener((observable, oldValue, newValue) -> {
//            JSONObject js = (JSONObject) newValue;
//            for (int i = 0; i < baseTaxrate.size(); i++) {
//                JSONObject jsonObject = baseTaxrate.getJSONObject(i);
//                if(jsonObject.getString("ID").equals(js.getString("TAXRATE"))){
//                    double amount = RateUtils.countRate(js.getDouble("AMOUNT"), jsonObject.getDoubleValue("TAX_TATE"));
//                    js.put("INPUT",amount);
//
//                    break;
//                }
//            }
//        });
        TAXRATE.setCellFactory(taxrate);
        TAXRATE.setCellValueFactory(new JSONObjectCell<String>("TAXRATE"));
        columns.add(TAXRATE);


        INPUT.setId("INPUT");
        INPUT.setText("税金");
        INPUT.setPrefWidth(120);
        INPUT.setEditable(true);
        INPUT.setCellFactory(new ZymNumberFieldTableFX("INPUT"));
        INPUT.setCellValueFactory(new JSONObjectCell<String>("INPUT"));
        columns.add(INPUT);



        TableColumn DESCRIPTION = new TableColumn();
        DESCRIPTION.setId("DESCRIPTION");
        DESCRIPTION.setText("备注");
        DESCRIPTION.setPrefWidth(200);
        DESCRIPTION.setCellFactory(new ZymTextFieldTableFX("DESCRIPTION"));
        DESCRIPTION.setCellValueFactory(new JSONObjectCell<String>("DESCRIPTION"));
        columns.add(DESCRIPTION);
        cLedgerPane.setContent(this.cledger);
    }
    private Button sledgerAdd;
    private void initSledger() {
        this.sledger = new TableView();
        this.sledger.setFocusModel(null);
        ObservableList columns = sledger.getColumns();
        this.sledger.setPrefHeight(305);
        TableColumn index = new TableColumn();
        index.setId(Grid.INDEX);
        index.setText("序号");
        index.setPrefWidth(50);
        index.setStyle("-fx-alignment: center;");
        index.setCellFactory(new IDCell<>());
        columns.add(index);

        //new 搜索框


        TableColumn ADD = new TableColumn();
        ADD.setId("ADD");
        ADD.setPrefWidth(60);
        ADD.setSortable(false);
        sledgerAdd = new Button();
        sledgerAdd.setDisable(!sledger.isEditable());
        ImageView imageView = new ImageView(Icon.ADD);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        sledgerAdd.setGraphic(imageView);
        sledgerAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ObservableList items = sledger.getItems();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("BASE_COST_MANAGE_ID",null);
                jsonObject.put("AMOUNT","0.0");
                jsonObject.put("TAXRATE","0.0");
                jsonObject.put("INPUT","0.0");
                items.add(jsonObject);
            }
        });
        ADD.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn param) {
                TableCell<Map<String, String>, Object> cell = new TableCell<Map<String, String>, Object>() {
                    protected  void   updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            this.setGraphic(null);
                        }else {
                            Button button1 = new Button();
                            button1.setDisable(!sledger.isEditable());
                            ImageView imageView = new ImageView(Icon.CENTEL);
                            imageView.setFitWidth(20);
                            imageView.setFitHeight(20);
                            button1.setGraphic(imageView);
                            button1.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    ObservableList items = sledger.getItems();
                                    items.remove(getIndex());
                                }
                            });
                            this.setGraphic(button1);
                        }
                    }

                };
                cell.setId("DEL");
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        ADD.setGraphic(sledgerAdd);
        columns.add(ADD);


        TableColumn CODE = new TableColumn();
        CODE.setId("BASE_COST_MANAGE_ID");
        CODE.setText("费用类型");
        CODE.setEditable(true);
        CODE.setPrefWidth(230);
        List list = BaseDataSetvice.toSelectModelList(initData.getJSONArray("BASE_COST_MANAGE_S"), "ID", "NAME");
        CODE.setCellFactory(new ZymSelectFx("BASE_COST_MANAGE_ID",list));////表格编辑 但是得双击 不好~
        CODE.setCellValueFactory(new JSONObjectCell<String>("BASE_COST_MANAGE_ID"));

        columns.add(CODE);
        TableColumn AMOUNT = new TableColumn();
        AMOUNT.setId("AMOUNT");
        AMOUNT.setText("金额");
        AMOUNT.setPrefWidth(120);
        AMOUNT.setEditable(true);
        AMOUNT.setCellFactory(new ZymNumberFieldTableFX("AMOUNT"));
        AMOUNT.setCellValueFactory(new JSONObjectCell<String>("AMOUNT"));
        columns.add(AMOUNT);

        TableColumn INPUT= new TableColumn();


        TableColumn TAXRATE = new TableColumn();
        TAXRATE.setId("TAXRATE");
        TAXRATE.setText("税率");
        TAXRATE.setPrefWidth(120);
        TAXRATE.setEditable(true);
        JSONArray baseTaxrate = initData.getJSONArray("BASE_TAXRATE");
        List list2 = BaseDataSetvice.toSelectModelList(baseTaxrate, "TAX_TATE", "TAX_TATE");
        ZymSelectFx taxrate = new ZymSelectFx("TAXRATE", list2);
//        this.sledger.getItems().addListener(new ListChangeListener() {
//            @Override
//            public void onChanged(Change c) {
//                System.out.println(c);
//            }
//        });
//        taxrate.setChangeListener((observable, oldValue, newValue) -> {
//            JSONObject js = (JSONObject) newValue;
//            for (int i = 0; i < baseTaxrate.size(); i++) {
//                JSONObject jsonObject = baseTaxrate.getJSONObject(i);
//                if(jsonObject.getString("ID").equals(js.getString("TAXRATE"))){
//                    double amount = RateUtils.countRate(js.getDouble("AMOUNT"), jsonObject.getDoubleValue("TAX_TATE"));
//                    js.put("INPUT",amount);
//
//                    break;
//                }
//            }
//        });
        TAXRATE.setCellFactory(taxrate);
        TAXRATE.setCellValueFactory(new JSONObjectCell<String>("TAXRATE"));
        columns.add(TAXRATE);


        INPUT.setId("INPUT");
        INPUT.setText("税金");
        INPUT.setPrefWidth(120);
        INPUT.setEditable(true);
        INPUT.setCellFactory(new ZymNumberFieldTableFX("INPUT"));
        INPUT.setCellValueFactory(new JSONObjectCell<String>("INPUT"));
        columns.add(INPUT);



        TableColumn DESCRIPTION = new TableColumn();
        DESCRIPTION.setId("DESCRIPTION");
        DESCRIPTION.setText("备注");
        DESCRIPTION.setPrefWidth(200);
        DESCRIPTION.setCellFactory(new ZymTextFieldTableFX("DESCRIPTION"));
        DESCRIPTION.setCellValueFactory(new JSONObjectCell<String>("DESCRIPTION"));
        columns.add(DESCRIPTION);
        sLedgerPane.setContent(this.sledger);
    }


    private void initData() {
        this.initData = instance.getMapData("/tmsorder/init");
    }
    private Button productAdd;
    void initPorduct(){
        this.product = new TableView();
        this.product.setFocusModel(null);
        ObservableList columns = product.getColumns();
        this.product.setPrefHeight(305);
        TableColumn index = new TableColumn();
        index.setId(Grid.INDEX);
        index.setText("序号");
        index.setPrefWidth(50);
        index.setStyle("-fx-alignment: center;");
        index.setCellFactory(new IDCell<>());
        columns.add(index);

        //new 搜索框


        TableColumn ADD = new TableColumn();
        ADD.setId("ADD");
        ADD.setPrefWidth(60);
        ADD.setSortable(false);
        productAdd = new Button();
        productAdd.setDisable(!product.isEditable());
        ImageView imageView = new ImageView(Icon.ADD);
        imageView.setFitWidth(20);
        imageView.setFitHeight(20);
        productAdd.setGraphic(imageView);
        productAdd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ObservableList items = product.getItems();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("NAME","");
                jsonObject.put("CODE","");
                jsonObject.put("NUMBER","1.0");
                jsonObject.put("ROUGT_WEIGHT","0.0");
                jsonObject.put("VOLUME","0.0");
                jsonObject.put("UNIT","个");
                jsonObject.put("NET_WEIGHT","0.0");
                jsonObject.put("VALUE","0.0");
                items.add(jsonObject);
            }
        });
        ADD.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn param) {
                TableCell<Map<String, String>, Object> cell = new TableCell<Map<String, String>, Object>() {
                    protected  void   updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            this.setGraphic(null);
                        }else {
                            Button button1 = new Button();
                            button1.setDisable(!product.isEditable());
                            ImageView imageView = new ImageView(Icon.CENTEL);
                            imageView.setFitWidth(20);
                            imageView.setFitHeight(20);
                            button1.setGraphic(imageView);
                            button1.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    ObservableList items = product.getItems();
                                    items.remove(getIndex());
                                }
                            });
                            this.setGraphic(button1);
                        }
                    }

                };
                cell.setId("DEL");
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
        ADD.setGraphic(productAdd);
        columns.add(ADD);


        TableColumn CODE = new TableColumn();
        CODE.setId("CODE");
        CODE.setText("货品代码");
        CODE.setEditable(true);
        CODE.setPrefWidth(230);
        CODE.setCellFactory(new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn param) {
                TableCell<Map<String, String>, Object> cell = new TableCell<Map<String, String>, Object>() {
                    protected  void   updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            this.setGraphic(null);
                        }else {
                            if(  this.getGraphic() == null){
                                ProductScan productScan = new ProductScan(BASE_ITEM_ID,new CallBack() {
                                    @Override
                                    public void run(JSONObject jsonObject) {
                                        jsonObject.put("NUMBER",1);
                                        product.getItems().set(getIndex(),jsonObject);
                                    }
                                });
                                productScan.addChangeListener(new ChangeListener() {
                                    @Override
                                    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                                        ObservableList<Map<String, String>> tableData = param.getTableView().getItems();
                                        if(tableData.size()-1>=getIndex()){
                                            ((Map)tableData.get(getIndex())).put("CODE", productScan.getValue());
                                        }
                                    }
                                });
                                this.setGraphic(productScan);

                            }
                            ProductScan graphic = (ProductScan) getGraphic();
                            ObservableList<Map<String, String>> tableData = param.getTableView().getItems();
                            Object o = ((Map) tableData.get(getIndex())).get("CODE");
                            if(o != null && StringUtils.isNotEmpty(o.toString())){
                                graphic.setValue(o.toString());
                            }
                        }
                    }

                };
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });////表格编辑 但是得双击 不好~
        CODE.setCellValueFactory(new JSONObjectCell<String>("CODE"));

        columns.add(CODE);

        TableColumn NAME = new TableColumn();
        NAME.setId("NAME");
        NAME.setText("货品名称");
        NAME.setPrefWidth(230);
        NAME.setEditable(true);
        NAME.setCellFactory(new ZymTextFieldTableFX("NAME"));
        NAME.setCellValueFactory(new JSONObjectCell<String>("NAME"));
        columns.add(NAME);

        TableColumn UNIT = new TableColumn();
        UNIT.setId("UNIT");
        UNIT.setText("单位");
        UNIT.setPrefWidth(80);
        UNIT.setEditable(true);
        UNIT.setCellFactory(new ZymNumberFieldTableFX("UNIT"));
        UNIT.setCellValueFactory(new JSONObjectCell<String>("UNIT"));
        columns.add(UNIT);

        TableColumn NUMBER = new TableColumn();
        NUMBER.setId("NUMBER");
        NUMBER.setText("数量");
        NUMBER.setPrefWidth(100);
        NUMBER.setEditable(true);
        NUMBER.setCellFactory(new ZymNumberFieldTableFX("NUMBER"));
        NUMBER.setCellValueFactory(new JSONObjectCell<String>("NUMBER"));
        columns.add(NUMBER);

        TableColumn ROUGT_WEIGHT = new TableColumn();
        ROUGT_WEIGHT.setId("ROUGT_WEIGHT");
        ROUGT_WEIGHT.setText("毛重");
        ROUGT_WEIGHT.setPrefWidth(100);
        ROUGT_WEIGHT.setEditable(true);
        ROUGT_WEIGHT.setCellFactory(new ZymNumberFieldTableFX("ROUGT_WEIGHT"));
        ROUGT_WEIGHT.setCellValueFactory(new JSONObjectCell<String>("ROUGT_WEIGHT"));
        columns.add(ROUGT_WEIGHT);

        TableColumn NET_WEIGHT = new TableColumn();
        NET_WEIGHT.setId("NET_WEIGHT");
        NET_WEIGHT.setText("净重");
        NET_WEIGHT.setPrefWidth(100);
        NET_WEIGHT.setEditable(true);
        NET_WEIGHT.setCellFactory(new ZymNumberFieldTableFX("NET_WEIGHT"));
        NET_WEIGHT.setCellValueFactory(new JSONObjectCell<String>("NET_WEIGHT"));
        columns.add(NET_WEIGHT);

        TableColumn VOLUME = new TableColumn();
        VOLUME.setId("VOLUME");
        VOLUME.setText("体积");
        VOLUME.setPrefWidth(100);
        VOLUME.setEditable(true);
        VOLUME.setCellFactory(new ZymNumberFieldTableFX("VOLUME"));
        VOLUME.setCellValueFactory(new JSONObjectCell<String>("VOLUME"));
        columns.add(VOLUME);

        TableColumn VALUE = new TableColumn();
        VALUE.setId("VALUE");
        VALUE.setText("货值");
        VALUE.setPrefWidth(100);
        VALUE.setEditable(true);
        VALUE.setCellFactory(new ZymNumberFieldTableFX("VALUE"));
        VALUE.setCellValueFactory(new JSONObjectCell<String>("VALUE"));
        columns.add(VALUE);
//
        TableColumn DESCRIPTION = new TableColumn();
        DESCRIPTION.setId("DESCRIPTION");
        DESCRIPTION.setText("备注");
        DESCRIPTION.setPrefWidth(200);
        DESCRIPTION.setCellFactory(new ZymTextFieldTableFX("DESCRIPTION"));
        DESCRIPTION.setCellValueFactory(new JSONObjectCell<String>("DESCRIPTION"));
        columns.add(DESCRIPTION);
        productPane.setContent(this.product);
    }

    /**
     * 锁定表单
     * @param is
     */
    public void lockFrom(boolean is){
        DESCRIPTION.setDisable(is);
        TRACK_MODEL.setDisable(is);
        DELIVER.setDisable(is);
        TASK.setDisable(is);
        BASE_VEHICLE_TYPE.setDisable(is);
        BACK_NUMBER.setDisable(is);
        ESTIMATE_ARRIVE_TIME.setDisable(is);
        ESTIMATE_DESPATCH_TIME.setDisable(is);
        TIME.setDisable(is);
        COSTOMER_CODE.setDisable(is);
        TRANSPORT_NATURE.setDisable(is);
        TRANSPORT_TYPE.setDisable(is);
        LTL.setDisable(is);
        BASE_ITEM_ID.setDisable(is);
        FNAME.setDisable(is);
        FPERSION.setDisable(is);
        FPHONE.setDisable(is);
        FAREA.setDisable(is);
        FADDRESS.setDisable(is);
        SADDRESS.setDisable(is);
        SAREA.setDisable(is);
        SPHONE.setDisable(is);
        SPERSION.setDisable(is);
        SNAME.setDisable(is);
        this.sledger.setEditable(!is);
        this.product.setEditable(!is);
        this.cledger.setEditable(!is);
        productAdd.setDisable(is);
        sledgerAdd.setDisable(is);
        cledgerAdd.setDisable(is);
    }

}
