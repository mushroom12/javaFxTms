package com.zym.application.controller.base.product;

import com.alibaba.fastjson.JSONObject;
import com.zym.application.service.BaseDataSetvice;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymField;
import com.zym.framework.textfield.NumberTextField;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseProductFromController implements Initializable {
    @FXML
    @ZymField
    public ZymComboBox BASE_ITEM_ID;
    @FXML
    @ZymField
    public ZymComboBox UNIT;
    @FXML
    @ZymField
    public ZymComboBox BASE_PRODUCT_TYPE_ID;
    @FXML
    @ZymField
    public NumberTextField ROUGT_WEIGHT;
    @FXML
    @ZymField
    public NumberTextField NET_WEIGHT;
    @FXML
    @ZymField
    public NumberTextField VOLUME;
    @FXML
    @ZymField
    public NumberTextField VALUE;
    @FXML
    @ZymField
    private TextField NAME;
    @FXML
    @ZymField
    private TextField CODE;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        BaseDataSetvice instance = BaseDataSetvice.getInstance();
        JSONObject mapData = instance.getMapData("/baseproduct/init");
        List list = BaseDataSetvice.toSelectModelList(mapData.getJSONArray("BASE_ITEM"), "ID", "NAME");
        BASE_ITEM_ID.getItems().setAll(list);
        List list2 = BaseDataSetvice.toSelectModelList(mapData.getJSONArray("BASE_UNIT_MANAGE"), "NAME", "NAME");
        UNIT.getItems().setAll(list2);
        List list3 = BaseDataSetvice.toSelectModelList(mapData.getJSONArray("BASE_PRODUCT_TYPE"), "ID", "NAME");
        BASE_PRODUCT_TYPE_ID.getItems().setAll(list3);
    }
}
