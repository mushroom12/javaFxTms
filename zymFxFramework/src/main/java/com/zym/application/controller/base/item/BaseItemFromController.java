package com.zym.application.controller.base.item;

import com.alibaba.fastjson.JSONArray;
import com.zym.application.column.BaseData;
import com.zym.application.service.BaseDataSetvice;
import com.zym.framework.area.Area;
import com.zym.framework.combobox.AddressAutoComplete;
import com.zym.framework.combobox.AutoComplete;
import com.zym.framework.combobox.QueryComboBox;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.form.ZymField;
import com.zym.framework.model.ParseModel;
import com.zym.framework.utlis.FromUtils;
import com.zym.framework.utlis.IviewParseUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseItemFromController implements Initializable {
    @FXML
    @ZymField
    public QueryComboBox BASE_CUSTOMER_SUPPLIER_ID;
    @FXML
    @ZymField
    public ZymComboBox TRANSPORT_TYPE;
    @FXML
    @ZymField
    public ZymComboBox LTL;
    @FXML
    @ZymField
    public CheckBox TASK;
    @FXML
    @ZymField
    public CheckBox DELIVER;
    @FXML
    @ZymField
    public ZymComboBox BASE_VEHICLE_TYPE;
    @FXML
    @ZymField
    public ZymComboBox TRACK_MODEL;
    @FXML
    @ZymField
    public TextField SALE;
    @FXML
    @ZymField
    public TextField BACK_NUMBER;
    @FXML
    @ZymField
    public TextField FNAME;
    @FXML
    @ZymField
    public TextField FPERSION;
    @FXML
    @ZymField
    public TextField FPHONE;
    @FXML
    @ZymField
    public Area FAREA;
    @FXML
    @ZymField
    public ComboBox FADDRESS;
    @FXML
    @ZymField
    private TextField NAME;
    @FXML
    @ZymField
    private TextField CODE;
    @FXML
    @ZymField
    private TextField DESCRIPTION;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FromUtils.initDataBook("TransportType",TRANSPORT_TYPE);
        FromUtils.initDataBook("TrackModel",TRACK_MODEL);
        FromUtils.initDataBook("LTL",LTL);
        new AddressAutoComplete(new AutoComplete<>(FADDRESS)); //加工类型
        ParseModel parseModel = IviewParseUtils.parseGrid(BaseData.CSQUERY, null);
        Map map = new HashMap<>();
        map.put("name","CUSTOMER");
        BASE_CUSTOMER_SUPPLIER_ID.initGrid(parseModel.getTableColumns(),"NAME","/basecustomersupplier/initTableSelect",map,"ID","NAME");
        BaseDataSetvice instance = BaseDataSetvice.getInstance();
        JSONArray arrayData = instance.getArrayData("/basevehicletype/init");
        List list = BaseDataSetvice.toSelectModelList(arrayData, "CODE", "CODE");
        BASE_VEHICLE_TYPE.getItems().setAll(list);
    }
}
