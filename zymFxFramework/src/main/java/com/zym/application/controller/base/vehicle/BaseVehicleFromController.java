package com.zym.application.controller.base.vehicle;

import com.alibaba.fastjson.JSONArray;
import com.zym.application.service.BaseDataSetvice;
import com.zym.framework.combobox.ZymComboBox;
import com.zym.framework.datetimepicker.DateTimePicker;
import com.zym.framework.form.ZymField;
import com.zym.framework.textfield.NumberTextField;
import com.zym.framework.utlis.FromUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;


/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-02-19
 */
public class BaseVehicleFromController implements Initializable {
    @FXML @ZymField
    public ZymComboBox TYPE;
    @FXML @ZymField
    public ZymComboBox VEHICLE_TYPE;
    @FXML @ZymField
    public TextField DEIVER_NAME;
    @FXML @ZymField
    public TextField DEIVER_PHONE;
    @FXML @ZymField
    public TextField DEIVER_CODE;
    @FXML @ZymField
    public TextField DEIVER_LICENSE;
    @FXML @ZymField
    public ZymComboBox GPS_SUPPLIER;
    @FXML @ZymField
    public TextField GPS_CODE;
    @FXML @ZymField
    public TextField SUPPLIER;
    @FXML @ZymField
    public TextField COLOR;
    @FXML @ZymField
    public TextField SIZE;
    @FXML @ZymField
    public NumberTextField MAX_VOLUME;
    @FXML @ZymField
    public NumberTextField MAX_WEIGHT;
    @FXML @ZymField
    public DateTimePicker INSURE_END_TIME;
    @FXML @ZymField
    public DateTimePicker CHECK_END_TIME;
    @FXML @ZymField
    private TextField CODE;
    @FXML @ZymField
    private TextField DESCRIPTION;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FromUtils.initDataBook("VehicleCarType",TYPE);
        FromUtils.initDataBook("GPSType",GPS_SUPPLIER);
        BaseDataSetvice instance = BaseDataSetvice.getInstance();
        JSONArray arrayData = instance.getArrayData("/basevehicletype/init");
        List list = BaseDataSetvice.toSelectModelList(arrayData, "CODE", "CODE");
        VEHICLE_TYPE.getItems().setAll(list);
    }
}
