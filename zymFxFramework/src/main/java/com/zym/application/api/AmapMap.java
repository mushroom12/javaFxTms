package com.zym.application.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zym.application.utils.HttpUtil;
import com.zym.application.utils.LogUtils;
import com.zym.framework.combobox.AddressQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 *
 * @author
 * @create 2021-03-03
 */
public class AmapMap  implements AddressQuery {
    private static final String HTTP_URL = "https://restapi.amap.com/v3/assistant/inputtips";
    private static final String KEY = "f2c029140643b11182afcbdbe2ebb388";
    @Override
    public List<String> query(String text) {
        List<String> list = new ArrayList<>();
        HashMap<String,Object> map = new HashMap();
        map.put("key",KEY);
        map.put("keywords",text);
        try {
            String s = HttpUtil.doGet(HTTP_URL, map);
            JSONObject jsonObject = JSONObject.parseObject(s);
            if(jsonObject.getIntValue("status") == 1 && jsonObject.getIntValue("count")>0){
                JSONArray tips = jsonObject.getJSONArray("tips");
                for (int i = 0; i < tips.size(); i++) {
                    JSONObject tip = tips.getJSONObject(i);
                    list.add(tip.getString("address"));
                }
            }
           LogUtils.println("地址联查:"+jsonObject.toJSONString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
