package com.zym.application.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * QQ 165149324
 * 臧英明
 * 简单工具类
 * @author
 * @create 2021-02-04
 */
public abstract class LogUtils{
    public static final long MAX_LENGTH = 5000;
    public static final List<String> list = new ArrayList();
    public static final String TITLE = "Zym_Simple_Debug ";

    public static void println(String str){
        StringBuilder sb = new StringBuilder();
        String dateTime = DateUtils.formatNow("yyyy-MM-dd HH:mm:ss");
        sb.append(TITLE).append(dateTime).append(":").append(str);
        String s = sb.toString();
        save(s);
        System.out.println(s);
    }
    public static void save(String str){
        if(list.size()>=5000){
            clear();
        }
        list.add(str);
    }
    public static void clear(){
        list.clear();
    }
    public static List<String> getList(){
        return list;
    }

    public static void saveFile(String path){

    }

}
